﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ExamApp;
using IspitApp.Models;
using IspitApp.Services;
using ToastNotifications;
using ToastNotifications.Messages;

namespace IspitApp
{
    /// <summary>
    /// Interaction logic for AddStudentProfessor.xaml
    /// </summary>
    public partial class AddStudentProfessor : Page
    {
        UserService userService = new UserService();
        public Exam SelectedExam { get; set; }
        public Tuple<List<Exam>, List<User>> ExamsUsers { get; set; }
        public TextBox SearchUsersTextBox { get; private set; }
        public TextBox SearchExamsTextBox { get; private set; }
        public List<User> UsersOnExamReserve { get; private set; }
        public List<Exam> ExamsReserve { get; private set; }
        public Notifier HomeNotifier { get; set; }

        private List<Exam> Exams;
        private List<User> Users;
        BindingList<User> usersBinding;
        BindingList<Exam> examsBinding;
        //public ObservableCollection<Exam> SourceList
        //{
        //    get { return cmbExams; }
        //    set { value = cmbExams; }
        //}
        //public Exam SelectedItem
        //{
        //    get { return SelectedExam; }
        //    set { SelectedExam = value; }
        //}
        //public event PropertyChangedEventHandler PropertyChanged;
        public AddStudentProfessor()
        {
            InitializeComponent();
            this.Loaded += AddStudentProfessor_Loaded;
            Btn_AddAssign.Click += Btn_AddStudent_Click;
            Btn_CancelAddStudent.Click += Btn_CancelAddStudent_Click;
            //usersBinding = new BindingList<User>(Users);
            
            // Professor identities doesn't exist but it appears
        }

        private void Btn_AddStudent_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Users[0].Roles.Where(r => r.RoleName == "Student") != null && Users[0].Roles.Where(r => r.RoleName == "Student").Count() == 1)
                {
                    if (StudentCombo.SelectedItem != null)
                    {
                        if (ExamsCombo.SelectedItem == null)
                        {
                            var status = userService.AddStudentProfToExam(StudentCombo.SelectedItem as User, SelectedExam as Exam);

                            Home.HomeNotifier.ShowSuccess("Student " + (StudentCombo.SelectedItem as User).FullName() + " is added to exam " + (SelectedExam as Exam).ExamName);
                        }
                        else
                        {
                            var status = userService.AddStudentProfToExam(StudentCombo.SelectedItem as User, ExamsCombo.SelectedItem as Exam);

                            Home.HomeNotifier.ShowSuccess("Student " + (StudentCombo.SelectedItem as User).FullName() + " is added to exam " + (ExamsCombo.SelectedItem as Exam).ExamName);

                        }

                    }
                    else
                    {
                        Home.HomeNotifier.ShowWarning("you have not selected a student");
                    }
                }
                else if (Users[0].Roles.Where(r => r.RoleName == "Professor") != null && Users[0].Roles.Where(r => r.RoleName == "Professor").Count() == 1)
                {
                    if (ProfessorCombo.SelectedItem != null)
                    {
                        if (ExamsCombo.SelectedItem == null)
                        {
                            var status = userService.AddStudentProfToExam(ProfessorCombo.SelectedItem as User, SelectedExam as Exam);
                            Home.HomeNotifier.ShowSuccess("Professor " + (ProfessorCombo.SelectedItem as User).FullName() + " is assigned to exam " + (SelectedExam as Exam).ExamName);

                        }
                        else
                        {
                            var status = userService.AddStudentProfToExam(ProfessorCombo.SelectedItem as User, ExamsCombo.SelectedItem as Exam);
                            Home.HomeNotifier.ShowSuccess("Professor " + (ProfessorCombo.SelectedItem as User).FullName() + " is assigned to exam " + (ExamsCombo.SelectedItem as Exam).ExamName);
                        }
                    }
                }
                else
                {
                    HomeNotifier.ShowWarning("The list of Users is Empty");
                }
            }
            catch (Exception e1)
            {
                Home.HomeNotifier.ShowError(e1.Message);
            }
        }
        private void Btn_CancelAddStudent_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.GoBack();
        }
        private void AddStudentProfessor_Loaded(object sender, EventArgs e)
        {
            Exams = ExamsUsers.Item1 as List<Exam>;
            Users = (ExamsUsers.Item2 as List<User>);
            searchExams.KeyUp += SearchExams_KeyUp;
            searchUsers.KeyUp += SearchUsers_KeyUp;
            usersBinding = new BindingList<User>(Users);
            examsBinding = new BindingList<Exam>(Exams);
            ExamNameText.Content = SelectedExam.ExamName;
            ExamNameText.HorizontalContentAlignment = HorizontalAlignment.Center;
            ExamNameText.VerticalContentAlignment = VerticalAlignment.Center;
            ExamsCombo.ItemsSource = examsBinding;
            ExamsCombo.HorizontalContentAlignment = HorizontalAlignment.Center;
            ExamsCombo.VerticalContentAlignment = VerticalAlignment.Center;
            StudentCombo.HorizontalContentAlignment = HorizontalAlignment.Center;
            StudentCombo.VerticalContentAlignment = VerticalAlignment.Center;
            ProfessorCombo.HorizontalContentAlignment = HorizontalAlignment.Center;
            ProfessorCombo.VerticalContentAlignment = VerticalAlignment.Center;
            if (Users[0].Roles.Where(r => r.RoleName == "Student") != null && Users[0].Roles.Where(r => r.RoleName == "Student").Count() == 1)
            {
                StudentCombo.ItemsSource = usersBinding;
                StudentCombo.Visibility = Visibility.Visible;
                ProfessorCombo.Visibility = Visibility.Hidden;
                Btn_AddAssign.Content = "Add Student";
                lblFilterStudProf.Content = "Filter Students by Name, or Index Number :";
                lblStudProf.Content = "Choose Student :";
                addAssign.Title = "Add Student To An Exam";
            }
            else if (Users[0].Roles.Where(r => r.RoleName == "Professor") != null && Users[0].Roles.Where(r => r.RoleName == "Professor").Count() == 1)
            {
                ProfessorCombo.ItemsSource = usersBinding;
                StudentCombo.Visibility = Visibility.Hidden;
                ProfessorCombo.Visibility = Visibility.Visible;
                Btn_AddAssign.Content = "Assign Professor";
                lblFilterStudProf.Content = "Filter Professors by Name :";
                lblStudProf.Content = "Choose Professor :";
                addAssign.Title = "Assign Professor To An Exam";
            }
            else
            {
                StudentCombo.Visibility = Visibility.Hidden;
                ProfessorCombo.Visibility = Visibility.Hidden;
                MessageBox.Show("The list of Users is Empty");
            }
        }

        private void FilterUsers()
        {
            if (UsersOnExamReserve == null)
            {
                User[] users = new User[usersBinding.Count];
                usersBinding.ToList().CopyTo(users);
                UsersOnExamReserve = users.ToList();
            }
            if (SearchUsersTextBox.Text == "")
            {
                usersBinding.AddNewList(UsersOnExamReserve.Where(u => u.FirstName != "0").ToList());
            }
            else
            {
                usersBinding.AddNewList(UsersOnExamReserve.Where(u => u.FullName().ToLower().Contains(SearchUsersTextBox.Text.ToLower()) || u.Email.ToLower().Contains(SearchUsersTextBox.Text.ToLower()) || u.IndexNo.ToLower().Contains(SearchUsersTextBox.Text.ToLower())).ToList());
            }
        }

        private void FilterExams()
        {
            if (ExamsReserve == null)
            {
                Exam[] exams = new Exam[examsBinding.Count];
                examsBinding.ToList().CopyTo(exams);
                ExamsReserve = exams.ToList();
            }
            if (SearchExamsTextBox.Text == "")
            {
                examsBinding.AddNewList(ExamsReserve.Where(e => e.ExamName != "0").ToList());
            }
            else
            {
                examsBinding.AddNewList(ExamsReserve.Where(e => e.ExamName.ToLower().Contains(SearchExamsTextBox.Text.ToLower()) || e.LevelOfStudies.ToLower().Contains(SearchExamsTextBox.Text.ToLower()) || e.Departments.ContainsDepartmentWithName(SearchExamsTextBox.Text.ToLower())).ToList());
            }
        }

        private void SearchUsers_KeyUp(object sender, KeyEventArgs e)
        {
            SearchUsersTextBox = (sender as TextBox);
            FilterUsers();
        }

        private void SearchExams_KeyUp(object sender, KeyEventArgs e)
        {
            SearchExamsTextBox = (sender as TextBox);
            FilterExams();
        }

        private class BindingSource
        {
        }
    }
}
