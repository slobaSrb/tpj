﻿using ExamApp;
using IspitApp.Models;
using IspitApp.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ToastNotifications;
using ToastNotifications.Lifetime;
using ToastNotifications.Position;
using ToastNotifications.Messages;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;

namespace IspitApp
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Page, INotifyPropertyChanged
    {
        UserService userService = new UserService();
        ExamService examService = new ExamService();

        public static Notifier LoginNotifier = new Notifier(cfg =>
        {
            cfg.PositionProvider = new WindowPositionProvider(
                parentWindow: Application.Current.MainWindow,
                corner: Corner.TopRight,
                offsetX: 10,
                offsetY: 10);

            cfg.LifetimeSupervisor = new TimeAndCountBasedLifetimeSupervisor(
                notificationLifetime: TimeSpan.FromSeconds(3),
                maximumNotificationCount: MaximumNotificationCount.FromCount(5));

            cfg.Dispatcher = Application.Current.Dispatcher;
        });

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(
        [CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public static MainWindow MainWindow { get; internal set; }
        public string AppName { get { return MainWindow.AppName; } set { MainWindow.AppName = value; OnPropertyChanged("AppName"); } }

        public Login()
        {
            InitializeComponent();
            MainWindow.PropertyChanged += MainWindow_PropertyChanged;
            MainWindow.AppName = "Exam App";
            //NewMethod();
            this.MinHeight = 350;
            this.Loaded += Login_Loaded;
            this.ShowsNavigationUI = true;
            //this.SizeChanged += Login_SizeChanged;

        }

        private void MainWindow_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("AppName"))
            {
                OnPropertyChanged("AppName");
            }
        }

        private void Login_Loaded(object sender, RoutedEventArgs e)
        {

            this.NavigationService.RemoveBackEntry();
            //Window window = Window.GetWindow(this);
            //window.SetBinding(Window.MinHeightProperty, new Binding() { Source = this.MinHeight });
            //window.SetBinding(Window.MinWidthProperty, new Binding() { Source = this.MinWidth });
        }




        private void BtnRegister_Click(object sender, RoutedEventArgs e)
        {
            Registration reg = new Registration();
            this.NavigationService.Navigate(reg);
        }

        private void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            string email = textBoxEmail.Text;
            string password = passwordBox1.Password;
            try
            {
                Home.LoggedInUser = userService.CheckIfUserLoggedIn(email, Base64Encode(password));
                Home.LoggedInUser.Password = password;
                if (Home.LoggedInUser != null)
                {
                    DateTime dateValue = DateTime.Now;
                    DateTime parseData;
                    string pattern = "yyyy-MM-ddTHH:mm:ss";
                    try
                    {
                        if (DateTime.TryParseExact(dateValue.ToString(pattern), pattern, null, DateTimeStyles.None, out parseData))
                        {
                            Home.Token = new Token();
                            Home.Token.StartTime = parseData;
                            Home.Token.EndTime = parseData.AddHours(6);
                        }
                        else
                        {
                            throw new Exception("Dates didn't parse");
                        }

                        Home.Token.TokenValue = Base64Encode(Home.LoggedInUser.Email.ToLower() + ":" + Home.LoggedInUser.Password);
                        Home.Token.TokenID = 0;
                        Home.Token.UserID = Home.LoggedInUser.UserID;

                        HttpStatusCode status = userService.SaveToken(Home.Token);
                        GoToHome();
                    }
                    catch (Exception e1)
                    {
                        passwordBox1.Password = "";
                        LoginNotifier.ShowError(e1.Message);
                    }
                }
                else
                {
                    passwordBox1.Password = "";
                    throw new Exception("You entered wrong email or password");
                }
            }
            catch (Exception e2)
            {
                passwordBox1.Password = "";
                LoginNotifier.ShowError(e2.Message);
            }
        }

        private void GoToHome()
        {
            Home homePage = new Home();
            LoginNotifier.ShowSuccess("User " + Home.LoggedInUser.FullName() + " successfully logged in");

            Home.MainWindow.AppName = "Exam App - " + Home.LoggedInUser.FullName();
            this.NavigationService.Navigate(homePage);
        }

        private static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        private static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}
//public Grid CreateLayout(Grid grid)
//{
//    grid.Children.Clear();
//    grid.ColumnDefinitions.Clear();
//    grid.RowDefinitions.Clear();
//    ColumnDefinition colMenu = new ColumnDefinition();
//    colMenu.Width = new GridLength(1, GridUnitType.Star);
//    grid.ColumnDefinitions.Add(colMenu);

//    ColumnDefinition colPanel = new ColumnDefinition();
//    colPanel.Width = new GridLength(9, GridUnitType.Star);
//    grid.ColumnDefinitions.Add(colPanel);


//    RowDefinition rowTitle = new RowDefinition();
//    rowTitle.Height = new GridLength(10, GridUnitType.Star);
//    grid.RowDefinitions.Add(rowTitle);


//    RowDefinition rowHome = new RowDefinition();
//    rowHome.Height = new GridLength(90, GridUnitType.Star);
//    grid.RowDefinitions.Add(rowHome);

//    WrapPanel leftMenu = new WrapPanel();
//    leftMenu.Background = new SolidColorBrush(Colors.CornflowerBlue);

//    Border border = new Border();
//    border.BorderThickness = new Thickness(17, 0, 0, 0);
//    border.BorderBrush = new SolidColorBrush(Colors.CornflowerBlue);
//    ScrollViewer scrollField = new ScrollViewer();
//    Grid mainPanel = new Grid();
//    mainPanel.Background = new SolidColorBrush(Colors.CornflowerBlue);
//    scrollField.VerticalScrollBarVisibility = ScrollBarVisibility.Visible;
//    scrollField.Content = mainPanel;
//    mainPanel.HorizontalAlignment = HorizontalAlignment.Stretch;
//    mainPanel.VerticalAlignment = VerticalAlignment.Stretch;
//    mainPanel.Width = 400;
//    mainPanel.Height = 300;
//    scrollField.HorizontalAlignment = HorizontalAlignment.Center;
//    scrollField.VerticalAlignment = VerticalAlignment.Center;
//    border.Child = scrollField;
//    Grid.SetRow(leftMenu, 0);
//    Grid.SetColumn(leftMenu, 0);
//    Grid.SetRowSpan(leftMenu, 2);

//    Grid.SetRow(border, 1);
//    Grid.SetColumn(border, 1);

//    grid.Children.Add(leftMenu);
//    grid.Children.Add(border);

//    (grid.Children[0] as WrapPanel).Orientation = Orientation.Vertical;
//    Button btnActivity = new Button();
//    //var brush = new ImageBrush();
//    //brush.ImageSource = new BitmapImage(new Uri("images/pmf_logo.jpg", UriKind.Relative));
//    //btnChat.Background = brush;
//    //btnActivity.Content = "Chat";
//    //btnActivity.Click += BtnChat_Click;
//    //leftMenu.Children.Add(btnActivity);

//    //Button btnExams = new Button();
//    //btnExams.Content = "Exams";
//    //btnExams.Click += BtnExams_Click;
//    //leftMenu.Children.Add(btnExams);

//    Button btnLogin = new Button();
//    btnLogin.Content = "Login";
//    btnLogin.Click += BtnLogin_Click;
//    leftMenu.Children.Add(btnLogin);

//    Label pageName = new Label();
//    Grid.SetRow(pageName, 0);
//    Grid.SetColumn(pageName, 1);
//    grid.Children.Add(pageName);
//    pageName.Background = new SolidColorBrush(Colors.CornflowerBlue);
//    pageName.HorizontalAlignment = HorizontalAlignment.Stretch;
//    pageName.VerticalAlignment = VerticalAlignment.Stretch;
//    pageName.HorizontalContentAlignment = HorizontalAlignment.Center;
//    pageName.VerticalContentAlignment = VerticalAlignment.Center;
//    pageName.Foreground = new SolidColorBrush(Colors.White);
//    return mainPanel;
//}