﻿using ExamApp.Models;
using IspitApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ExamApp
{
    public static class ExtensionMethods
    {
        public static bool ContainsRoleWithName(this ICollection<Role> roles, string roleName)
        {
            bool contains = false;

            foreach (var item in roles)
            {
                if (item.RoleName.Equals(roleName))
                {
                    contains = true;
                    break;
                }
            }
            return contains;
        }

        public static bool ContainsDepartmentWithName(this ICollection<Department> departs, string depName)
        {
            bool contains = false;

            foreach (var item in departs)
            {
                if (item.DepartmentName.ToLower().Contains(depName.ToLower()))
                {
                    contains = true;
                    break;
                }
            }
            return contains;
        }

        public static string ToStringDeps(this ICollection<Department> departs)
        {
            string rez = "Departments :\n";

            foreach (var item in departs)
            {
                rez += item.DepartmentName + "\n";
            }
            return rez.Trim();
        }

        public static void AddNewList<T>(this BindingList<T> bindingList, List<T> list)
        {
            bindingList.Clear();
            foreach (var item in list)
            {
                bindingList.Add(item);
            }
        }

        public static ObservableCollection<T> ToObservable<T>(this List<T> list)
        {
            var oc = new ObservableCollection<T>();
            foreach (var item in list)
            {
                oc.Add(item);
            }
            return oc;
        }
        public static WrapPanel CreateLayout(this Grid grid)
        {
            grid.Children.Clear();
            grid.ColumnDefinitions.Clear();
            grid.RowDefinitions.Clear();
            ColumnDefinition colMenu = new ColumnDefinition();
            colMenu.Width = new GridLength(1, GridUnitType.Star);
            grid.ColumnDefinitions.Add(colMenu);

            ColumnDefinition colPanel = new ColumnDefinition();
            colPanel.Width = new GridLength(9, GridUnitType.Star);
            grid.ColumnDefinitions.Add(colPanel);


            RowDefinition rowTitle = new RowDefinition();
            rowTitle.Height = new GridLength(10, GridUnitType.Star);
            grid.RowDefinitions.Add(rowTitle);


            RowDefinition rowHome = new RowDefinition();
            rowHome.Height = new GridLength(90, GridUnitType.Star);
            grid.RowDefinitions.Add(rowHome);

            WrapPanel leftMenu = new WrapPanel();
            leftMenu.Background = new SolidColorBrush(Colors.CornflowerBlue);

            Border border = new Border();
            border.BorderThickness = new Thickness(17, 0, 0, 0);
            border.BorderBrush = new SolidColorBrush(Colors.CornflowerBlue);
            ScrollViewer scrollField = new ScrollViewer();
            WrapPanel mainPanel = new WrapPanel();
            mainPanel.Background = new SolidColorBrush(Colors.CornflowerBlue);
            scrollField.VerticalScrollBarVisibility = ScrollBarVisibility.Visible;
            scrollField.Content = mainPanel;

            border.Child = scrollField;
            Grid.SetRow(leftMenu, 0);
            Grid.SetColumn(leftMenu, 0);
            Grid.SetRowSpan(leftMenu, 2);

            Grid.SetRow(border, 1);
            Grid.SetColumn(border, 1);

            grid.Children.Add(leftMenu);
            grid.Children.Add(border);

            (grid.Children[0] as WrapPanel).Orientation = Orientation.Vertical;
            Button btnActivity = new Button();
            //var brush = new ImageBrush();
            //brush.ImageSource = new BitmapImage(new Uri("images/pmf_logo.jpg", UriKind.Relative));
            //btnChat.Background = brush;
            btnActivity.Content = "Chat";
            btnActivity.Click += BtnChat_Click;
            leftMenu.Children.Add(btnActivity);

            Button btnExams = new Button();
            btnExams.Content = "Exams";
            btnExams.Click += BtnExams_Click;
            leftMenu.Children.Add(btnExams);

            Button btnLogin = new Button();
            btnLogin.Content = "Login";
            btnLogin.Click += BtnLogin_Click;
            leftMenu.Children.Add(btnLogin);

            Button btnLogout = new Button();
            btnLogout.Content = "Logout";
            btnLogout.Click += BtnLogout_Click;
            leftMenu.Children.Add(btnLogout);

            Button btnSettings = new Button();
            btnSettings.Content = "Settings";
            btnSettings.Click += BtnSettings_Click;
            leftMenu.Children.Add(btnSettings);


            Label pageName = new Label();
            Grid.SetRow(pageName, 0);
            Grid.SetColumn(pageName, 1);
            grid.Children.Add(pageName);
            pageName.Background = new SolidColorBrush(Colors.CornflowerBlue);
            pageName.HorizontalAlignment = HorizontalAlignment.Stretch;
            pageName.VerticalAlignment = VerticalAlignment.Stretch;
            pageName.HorizontalContentAlignment = HorizontalAlignment.Center;
            pageName.VerticalContentAlignment = VerticalAlignment.Center;
            pageName.Foreground = new SolidColorBrush(Colors.White);
            mainPanel.Orientation = Orientation.Horizontal;
            return mainPanel;
        }

        private static void BtnLogout_Click(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }
        private static void BtnSettings_Click(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private static void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private static void BtnChat_Click(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }
        private static void BtnExams_Click(object sender, RoutedEventArgs e)
        {

        }



        //public static WrapPanel CreateLayout1(this Grid grid)
        //{
        //    grid.Children.Clear();
        //    grid.ColumnDefinitions.Clear();
        //    grid.RowDefinitions.Clear();
        //    ColumnDefinition colMenu = new ColumnDefinition();
        //    colMenu.Width = new GridLength(1, GridUnitType.Star);
        //    grid.ColumnDefinitions.Add(colMenu);

        //    ColumnDefinition colPanel = new ColumnDefinition();
        //    colPanel.Width = new GridLength(9, GridUnitType.Star);
        //    grid.ColumnDefinitions.Add(colPanel);


        //    RowDefinition rowTitle = new RowDefinition();
        //    rowTitle.Height = new GridLength(10, GridUnitType.Star);
        //    grid.RowDefinitions.Add(rowTitle);


        //    RowDefinition rowHome = new RowDefinition();
        //    rowHome.Height = new GridLength(90, GridUnitType.Star);
        //    grid.RowDefinitions.Add(rowHome);

        //    WrapPanel leftMenu = new WrapPanel();
        //    leftMenu.Background = new SolidColorBrush(Colors.CornflowerBlue);
        //    Border border = new Border();
        //    ScrollViewer scrollField = new ScrollViewer();
        //    border.Child = scrollField;
        //    border.BorderThickness = new Thickness(4);
        //    border.BorderBrush = new SolidColorBrush(Colors.CornflowerBlue);
        //    WrapPanel mainPanel = new WrapPanel();
        //    mainPanel.Background = new SolidColorBrush(Colors.CornflowerBlue);
        //    scrollField.VerticalScrollBarVisibility = ScrollBarVisibility.Visible;
        //    scrollField.Content = mainPanel;

        //    Grid.SetRow(leftMenu, 0);
        //    Grid.SetColumn(leftMenu, 0);
        //    Grid.SetRowSpan(leftMenu, 2);

        //    Grid.SetRow(border, 1);
        //    Grid.SetColumn(border, 1);

        //    grid.Children.Add(leftMenu);
        //    grid.Children.Add(border);

        //    (grid.Children[0] as WrapPanel).Orientation = Orientation.Vertical;
        //    Button btnActivity = new Button();
        //    //var brush = new ImageBrush();
        //    //brush.ImageSource = new BitmapImage(new Uri("images/pmf_logo.jpg", UriKind.Relative));
        //    //btnActivity.Background = brush;
        //    btnActivity.Content = "Chat";
        //    btnActivity.Click += BtnChat_Click;
        //    leftMenu.Children.Add(btnActivity);

        //    Button btnLogin = new Button();
        //    btnLogin.Content = "Login";
        //    btnLogin.Click += BtnLogin_Click; ;
        //    leftMenu.Children.Add(btnLogin);

        //    Button btnLogout = new Button();
        //    btnLogout.Content = "Logout";
        //    btnLogout.Click += BtnLogout_Click;
        //    leftMenu.Children.Add(btnLogout);


        //    Label pageName = new Label();
        //    Grid.SetRow(pageName, 0);
        //    Grid.SetColumn(pageName, 1);
        //    grid.Children.Add(pageName);
        //    pageName.Background = new SolidColorBrush(Colors.CornflowerBlue);
        //    pageName.HorizontalAlignment = HorizontalAlignment.Stretch;
        //    pageName.VerticalAlignment = VerticalAlignment.Stretch;
        //    pageName.HorizontalContentAlignment = HorizontalAlignment.Center;
        //    pageName.VerticalContentAlignment = VerticalAlignment.Center;
        //    pageName.Foreground = new SolidColorBrush(Colors.White);
        //    mainPanel.Orientation = Orientation.Horizontal;
        //    return mainPanel;
        //}

    }
}
