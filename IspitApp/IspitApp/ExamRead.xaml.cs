﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Ionic.Zip;
using IspitApp.Services;
using IspitApp.Models;
using ExamApp;
using ToastNotifications.Messages;
using ExamApp.ChatApp.Server;
using System.Threading.Tasks;
using System.Threading;
using Newtonsoft.Json;
using System.IO;
using System.Net.Http;

namespace IspitApp
{
    /// <summary>
    /// Interaction logic for IspitRead.xaml
    /// </summary>
    public partial class ExamRead : Page
    {
        public static Exam SelectedExam { get; set; }
        public Button CheckInButton { get; set; }
        public Button ChatButton { get; set; }
        public Button BtnUpload { get; set; }
        public Button BtnDownload { get; set; }
        public Window TickingTime { get; set; }
        public Button BtnLeftMenuChat { get; set; }
        public Button BtnLeftMenuExams { get; set; }
        public Button BtnLeftMenuLogIn { get; set; }
        public Button BtnLeftMenuLogOut { get; set; }
        public Button BtnLeftMenuSettings { get; set; }
        public User LoggedInUser { get; set; }
        public SolidColorBrush DefaultButtonColor { get; set; }

        ExamService examService = new ExamService();
        UserService userService = new UserService();
        public List<Exam> Exams { get; set; }
        public List<User> Students { get; set; }
        public List<User> Professors { get; set; }
        public StackPanel StackPanel1 { get; private set; }
        public static Dictionary<int, Thread> ExamChatThreads = new Dictionary<int, Thread>();

        //public Props Props { get; set; }

        public ExamRead()
        {
            InitializeComponent();
            InitializeLeftMenu();
            CreateLayout();
            this.SizeChanged += Home_SizeChangedExam;
            //this. += ExamRead_Initialized; //+= NavigationService_LoadCompleted;
            this.Loaded += ExamRead_Loaded;

        }

        private void LoadData()
        {
            //Exams = examService.GetAllExams();

        }

        private void ExamRead_Loaded(object sender, RoutedEventArgs e)
        {

            //Window window = Window.GetWindow(this);
            //window.SetBinding(Window.MinHeightProperty, new Binding() { Source = this.MinHeight });
            //window.SetBinding(Window.MinWidthProperty, new Binding() { Source = this.MinWidth });
            //(exam.Children[2] as Label).Content = SelectedExam.ExamName + " - Exam Options";
        }

        private void Home_SizeChangedExam(object sender, SizeChangedEventArgs e)
        {
            foreach (var child in (ExamWrapPanel).Children)
            {
                (child as Button).Width = e.NewSize.Width / 6.2;
                (child as Button).Height = e.NewSize.Width / 6.2;
                (((child as Button).Content as StackPanel).Children[0] as Image).Width = e.NewSize.Width / 6.2 - e.NewSize.Width / 6.2 * 30 / 100;
                (((child as Button).Content as StackPanel).Children[0] as Image).Height = e.NewSize.Width / 6.2 - e.NewSize.Width / 6.2 * 30 / 100;
            }

            (scrollField).BorderThickness = new Thickness(15, 0, 0, 0);

            foreach (var child in (exam.Children[0] as WrapPanel).Children)
            {
                (child as Button).Width = e.NewSize.Width / 10;
                (child as Button).Height = e.NewSize.Width / 10;
            }
        }


        private void InitializeLeftMenu()
        {
            BtnLeftMenuChat = new Button();
            BtnLeftMenuChat.Content = "Chat";
            BtnLeftMenuChat.Click += BtnChat_Click;
            leftMenu.Children.Add(BtnLeftMenuChat);

            BtnLeftMenuExams = new Button();
            BtnLeftMenuExams.Content = "Exams";
            BtnLeftMenuExams.Click += BtnExams_Click;
            leftMenu.Children.Add(BtnLeftMenuExams);
            leftMenu.Background = new SolidColorBrush(Colors.CornflowerBlue);

            BtnLeftMenuLogOut = new Button();
            BtnLeftMenuLogOut.Content = "Logout";
            BtnLeftMenuLogOut.Click += BtnLogout_Click;
            leftMenu.Children.Add(BtnLeftMenuLogOut);

            if (Home.LoggedInUser.Roles.ContainsRoleWithName("Administrator") || Home.LoggedInUser.Roles.ContainsRoleWithName("Professor"))
            {
                Button BtnAllExams = new Button();
                BtnAllExams.Content = "All Exams";
                BtnAllExams.Click += BtnAllExams_Click;
                leftMenu.Children.Add(BtnAllExams);

                Button BtnAllUsers = new Button();
                BtnAllUsers.Content = "All Users";
                BtnAllUsers.Click += BtnAllUsers_Click;
                leftMenu.Children.Add(BtnAllUsers);
            }
        }

        public void CreateLayout()
        {
            ExamWrapPanel.Children.Clear();




            border.BorderThickness = new Thickness(17, 0, 0, 0);
            border.BorderBrush = new SolidColorBrush(Colors.CornflowerBlue);

            ExamWrapPanel.Background = new SolidColorBrush(Colors.CornflowerBlue);
            scrollField.VerticalScrollBarVisibility = ScrollBarVisibility.Visible;
            scrollField.Content = ExamWrapPanel;


            (ExamWrapPanel).Orientation = Orientation.Vertical;
            List<Tuple<BitmapImage, string>> opcije = new List<Tuple<BitmapImage, string>>();
            opcije.Add(new Tuple<BitmapImage, string>(new BitmapImage(new Uri("/images/arrival_logo.png", UriKind.Relative)), "arrival check-in"));
            opcije.Add(new Tuple<BitmapImage, string>(new BitmapImage(new Uri("/images/chat_logo.jpg", UriKind.Relative)), "chat"));
            opcije.Add(new Tuple<BitmapImage, string>(new BitmapImage(new Uri("/images/upload_logo.png", UriKind.Relative)), "upload files"));
            opcije.Add(new Tuple<BitmapImage, string>(new BitmapImage(new Uri("/images/upload_logo.png", UriKind.Relative)), "download files"));
            opcije.Add(new Tuple<BitmapImage, string>(new BitmapImage(new Uri("/images/add_student_logo.png", UriKind.Relative)), "add student"));
            opcije.Add(new Tuple<BitmapImage, string>(new BitmapImage(new Uri("/images/see_students_logo.png", UriKind.Relative)), "see all students"));
            opcije.Add(new Tuple<BitmapImage, string>(new BitmapImage(new Uri("/images/professor_logo.png", UriKind.Relative)), "assign professor"));


            ExamWrapPanel.Orientation = Orientation.Horizontal;
            for (int i = 0; i < opcije.Count; i++)
            {
                if ((Home.LoggedInUser.Roles.ContainsRoleWithName("Professor") || Home.LoggedInUser.Roles.ContainsRoleWithName("Administrator")))
                {
                    if (Home.LoggedInUser.Roles.ContainsRoleWithName("Professor") && opcije[i].Item2.Equals("arrival check-in")) { continue; }
                    Button btnOption = new Button();
                    StackPanel spPom = new StackPanel();
                    var img = new Image();
                    spPom.Children.Add(img);
                    var tb = new TextBlock();
                    spPom.Children.Add(tb);
                    img.Source = opcije[i].Item1;
                    tb.Text = opcije[i].Item2;
                    btnOption.Click += BtnExam_OptionChoose;

                    btnOption.Content = spPom;
                    ExamWrapPanel.Children.Add(btnOption);
                    if (opcije[i].Item2.Equals("chat"))
                    {
                        ChatButton = btnOption;
                        ChatButton.Click += ChatButton_Click;
                        if (Home.LoggedInUser.Roles.ContainsRoleWithName("Student")) ChatButton.IsEnabled = true;
                    }
                    if (opcije[i].Item2.Equals("download files")) { BtnDownload = btnOption; }
                    if (opcije[i].Item2.Equals("upload files")) { BtnUpload = btnOption; BtnUpload.IsEnabled = true; }
                }
                else if (!(opcije[i].Item2 == "add student" || opcije[i].Item2 == "see all students" || opcije[i].Item2 == "assign professor" || opcije[i].Item2 == "download files"))
                {
                    Button btnOption = new Button();
                    StackPanel spPom = new StackPanel();
                    spPom.Children.Add(new Image());
                    spPom.Children.Add(new TextBlock());
                    (spPom.Children[0] as Image).Source = opcije[i].Item1;
                    (spPom.Children[1] as TextBlock).Text = opcije[i].Item2;
                    btnOption.Click += BtnExam_OptionChoose;

                    btnOption.Content = spPom;
                    ExamWrapPanel.Children.Add(btnOption);

                    if (opcije[i].Item2.Equals("arrival check-in")) { CheckInButton = btnOption; CheckInButton.IsEnabled = true; }
                    if (opcije[i].Item2.Equals("chat"))
                    {
                        ChatButton = btnOption;
                        ChatButton.Click += ChatButton_Click;
                        if (Home.LoggedInUser.Roles.ContainsRoleWithName("Student")) ChatButton.IsEnabled = false;
                    }
                    if (opcije[i].Item2.Equals("upload files")) { BtnUpload = btnOption; BtnUpload.IsEnabled = false; }

                }
            }

            StackPanel1 = new StackPanel();
            lblTitle1.Content = StackPanel1;
            StackPanel1.Orientation = Orientation.Horizontal;
            var lbl = new Label() { Content = SelectedExam.ExamName + " - Exam Options" };
            StackPanel1.Children.Add(lbl);
            lblTitle1.Background = new SolidColorBrush(Colors.CornflowerBlue);
            lblTitle1.HorizontalAlignment = HorizontalAlignment.Stretch;
            lblTitle1.VerticalAlignment = VerticalAlignment.Stretch;
            lblTitle1.HorizontalContentAlignment = HorizontalAlignment.Center;
            lblTitle1.VerticalContentAlignment = VerticalAlignment.Center;
            lblTitle1.Foreground = new SolidColorBrush(Colors.White);
            ExamWrapPanel.Orientation = Orientation.Horizontal;
        }

        private void BtnAllExams_Click(object sender, RoutedEventArgs e)
        {
            SeeAllExams allExams = new SeeAllExams();
            this.NavigationService.Navigate(allExams);
        }

        private void BtnAllUsers_Click(object sender, RoutedEventArgs e)
        {
            SeeAllUsers allUsers = new SeeAllUsers();
            this.NavigationService.Navigate(allUsers);
        }

        private async void BtnExam_OptionChoose(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            StackPanel sp = (StackPanel)button.Content;
            TextBlock tb = (TextBlock)sp.Children[1];
            if (tb.Text.Equals("upload files"))
            {
                try
                {
                    OpenFileDialog ofd = new OpenFileDialog();
                    ofd.Title = "Choose files to upload";
                    ofd.Filter = "PDF-s and Documents (*.doc, *.docx, *.pdf) | *.doc; *.docx; *.pdf | Archives (*.zip, *.7z, *.rar)|*.zip;*.7z;*.rar|All files (*.*)|*.*";
                    ofd.InitialDirectory = Environment.CurrentDirectory;
                    ofd.RestoreDirectory = true;
                    ofd.Multiselect = true;
                    var result = ofd.ShowDialog();
                    string fileName = Home.LoggedInUser.FullName().Replace(" ", "_") + "_" + Home.LoggedInUser.IndexNo + "_" + DateTime.Now.ToString("dd/MM/yy-H/mm/ss") + ".zip";// Ime Prezime brIndexa predmet rok " + DateTime.Now.ToString("dd/MM/yy - H/mm/ss") + ".zip";

                    if (result == true)
                    {
                        var stream = new System.IO.MemoryStream();
                        using (ZipFile zip = new ZipFile())
                        {
                            foreach (var file in ofd.FileNames)
                            {
                                //zip.AddFile(file);
                                zip.AddFile(file, fileName);
                            }
                            zip.Save(fileName);
                            //examService.UploadFile(Home.CheckIn, zip, fileName);
                            //zip.Save(stream);
                        }
                        byte[] data = stream.ToArray();

                        //implementirati da se fajl uploaduje kod profesora

                        //implementirati checkout

                        BtnLeftMenuExams.Content = "Exams";
                        BtnLeftMenuExams.Click += BtnExams_Click;
                        BtnLeftMenuExams.Background = DefaultButtonColor;
                        //TickingTime.Close();
                        CheckIn checkIn;
                        if (!Home.LoggedInUser.Roles.ContainsRoleWithName("Professor"))
                        {
                            checkIn = Home.CheckIn;
                        }
                        else
                        {
                            checkIn = new CheckIn();
                        }
                        if (Home.LoggedInUser.Roles.ContainsRoleWithName("Professor"))
                        {
                            checkIn.CheckInID = 0;
                            checkIn.CheckInDateTime = DateTime.Now;
                            checkIn.ExamID = ExamRead.SelectedExam.ExamID;
                            checkIn.UserID = Home.LoggedInUser.UserID;

                        }
                        else
                        {
                            CheckInButton.Click += BtnExam_OptionChoose;
                            CheckInButton.Click -= CheckOutButtonClick;
                            ((CheckInButton.Content as StackPanel).Children[1] as TextBlock).Text = "arrival check-in";

                            CheckInButton.Background = DefaultButtonColor;
                        }
                        if (Home.LoggedInUser.Roles.ContainsRoleWithName("Student")) ChatButton.IsEnabled = false;
                        this.ShowsNavigationUI = true;
                        if (Home.LoggedInUser.Roles.ContainsRoleWithName("Student")) BtnLeftMenuChat.IsEnabled = false;
                        BtnLeftMenuLogOut.IsEnabled = true;


                        Home.HomeNotifier.ShowSuccess("File Zipped");
                        //checkIn.ExamID = ExamRead.SelectedExam.ExamID;
                        //checkIn.UserID = Home.LoggedInUser.UserID;
                        //not important
                        Byte[] bytes = File.ReadAllBytes("..\\Debug\\" + fileName);
                        String file1 = Convert.ToBase64String(bytes);

                        checkIn.FilePath = file1;
                        //checkIn.CheckInID = 0;
                        //checkIn.CheckInDateTime = DateTime.Now;
                        checkIn.CheckOutDateTime = DateTime.Now;
                        if (!Home.LoggedInUser.Roles.ContainsRoleWithName("Professor"))
                        {
                            HttpResponseMessage msg = examService.UploadFile(checkIn);
                        }
                        else
                        {
                            HttpResponseMessage msg = examService.UploadFileProf(checkIn);
                        }
                        Home.HomeNotifier.ShowSuccess("File Uploaded");
                        if (!Home.LoggedInUser.Roles.ContainsRoleWithName("Professor"))
                        {
                            CheckOut();
                        }
                    }
                    else
                    {
                        Home.HomeNotifier.ShowWarning("You Choose To Cancel Uploading");
                    }
                }
                catch (Exception e1)
                {
                    Home.HomeNotifier.ShowError(e1.Message);
                }
            }
            else if (tb.Text.Equals("add student"))
            {
                try
                {
                    Students = userService.GetAllUsersWithRole("Student");
                    AddStudentProfessor addStudent = new AddStudentProfessor();
                    addStudent.ExamsUsers = new Tuple<List<Exam>, List<User>>(Exams, Students);
                    addStudent.SelectedExam = SelectedExam;
                    this.NavigationService.Navigate(addStudent);
                }
                catch (Exception e1)
                {
                    Home.HomeNotifier.ShowError(e1.Message);
                }
            }
            else if (tb.Text.Equals("see all students"))
            {
                try
                {
                    SeeUsersForExam seeUsers = new SeeUsersForExam();
                    seeUsers.StudentsOnExam = userService.GetStudentsForExam(SelectedExam.ExamID);//.ToObservable();
                    seeUsers.ExamOnSeeUsers = SelectedExam;
                    this.NavigationService.Navigate(seeUsers);
                }
                catch (Exception e3)
                {
                    Home.HomeNotifier.ShowError(e3.Message);
                }
            }
            else if (tb.Text.Equals("assign professor"))
            {
                try
                {
                    Professors = userService.GetAllUsersWithRole("Professor");
                    AddStudentProfessor assignProfessor = new AddStudentProfessor();
                    assignProfessor.ExamsUsers = new Tuple<List<Exam>, List<User>>(Exams, Professors);
                    assignProfessor.SelectedExam = SelectedExam;
                    this.NavigationService.Navigate(assignProfessor);
                }
                catch (Exception e2)
                {
                    Home.HomeNotifier.ShowError(e2.Message);
                }
            }
            else if (tb.Text.Equals("arrival check-in"))
            {
                try
                {
                    var result = examService.CheckIn(SelectedExam);
                    if (result.IsSuccessStatusCode)
                    {
                        Home.HomeNotifier.ShowSuccess("Student " + LoggedInUser.FullName() + " successfuly checked in for exam " + SelectedExam.ExamName);
                        button.Background = new SolidColorBrush(Colors.LightGreen);
                        BtnLeftMenuExams.Content = new Label() { Content = "Start : " + DateTime.Now.ToShortTimeString() };
                        BtnLeftMenuExams.Click -= BtnExams_Click;
                        DefaultButtonColor = BtnLeftMenuExams.Background as SolidColorBrush;
                        BtnLeftMenuExams.Background = new SolidColorBrush(Colors.LightGreen);
                        //    TickingTime = new Window() { Height = 100, Width = 200, Background = new SolidColorBrush(Colors.LightGreen) };
                        //TickingTime.Content = new Label() { Content = "Start : " + DateTime.Now.ToShortTimeString() };
                        //TickingTime.Show();
                        CheckInButton.Click -= BtnExam_OptionChoose;
                        CheckInButton.Click += CheckOutButtonClick;

                        tb.Text = "check - out";

                        BtnUpload.IsEnabled = true;
                        ChatButton.IsEnabled = true;
                        Home.CheckIn = JsonConvert.DeserializeObject<CheckIn>(result.Content.ReadAsStringAsync().Result) as CheckIn;

                        ChatButton.Click += ChatButton_Click;
                        this.ShowsNavigationUI = false;
                        BtnLeftMenuChat.IsEnabled = true;
                        BtnLeftMenuLogOut.IsEnabled = false;
                    }
                    else if (result.StatusCode == System.Net.HttpStatusCode.Forbidden)
                    {
                        Home.HomeNotifier.ShowError(result.Content.ReadAsStringAsync().Result);
                        CheckOut();
                    }
                    else
                    {
                        button.Background = new SolidColorBrush(Colors.Red);
                        Home.HomeNotifier.ShowError("Checking in didn't succeed");
                    }
                }
                catch (Exception e4)
                {
                    Home.HomeNotifier.ShowError(e4.Message);
                }
            }
            else if (tb.Text.Equals("download files"))
            {
                try
                {
                    DownloadFiles download = new DownloadFiles();
                    download.ExamOnDownloadFiles = SelectedExam;
                    this.NavigationService.Navigate(download);
                }
                catch (Exception e4)
                {
                    Home.HomeNotifier.ShowError(e4.Message);
                }
            }
        }

        private void StartServer()
        {
            ChatServer.StartServer();
        }

        private void ChatButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!ExamChatThreads.ContainsKey(SelectedExam.ExamID) && Home.LoggedInUser.Roles.ContainsRoleWithName("Professor"))
                {
                    var thread = new Thread(StartServer);
                    ExamChatThreads.Add(SelectedExam.ExamID, thread);
                    thread.IsBackground = true;
                    thread.Start();
                }

                ChatPage chat = new ChatPage();
                NavigationService.Navigate(chat);
            }
            catch (Exception e2)
            {
                Home.HomeNotifier.ShowError(e2.Message);
            }
        }

        private void CheckOutButtonClick(object sender, RoutedEventArgs e)
        {
            CheckOut();
        }

        private void CheckOut()
        {
            try
            {
                var result = examService.CheckOut();
                if (!result.IsSuccessStatusCode)
                {
                    Home.HomeNotifier.ShowError(result.Content.ReadAsStringAsync().Result);
                }
                Home.HomeNotifier.ShowSuccess(result.Content.ReadAsStringAsync().Result);
                userService.LogOut();
                Home.HomeNotifier.ShowSuccess("User " + Home.LoggedInUser.FullName() + " successfuly logged out");
                Login login = new Login();
                this.NavigationService.Navigate(login);
            }
            catch (Exception ex)
            {
                Home.HomeNotifier.ShowError(ex.Message);
            }
        }

        private void BtnChat_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ChatPage chat = new ChatPage();
                NavigationService.Navigate(chat);
            }
            catch (NotImplementedException e2)
            {
                Home.HomeNotifier.ShowError(e2.Message);
            }
        }
        private void BtnExams_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.NavigationService.CanGoBack)
                {
                    this.NavigationService.GoBack();
                }
                else
                {
                    Home home = new Home();
                    this.NavigationService.Navigate(home);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void BtnLogout_Click(object sender, RoutedEventArgs e)
        {
            LogOut();
        }

        private void LogOut()
        {
            try
            {
                User loggedOutUser = userService.LogOut();
                Home.HomeNotifier.ShowSuccess("User " + Home.LoggedInUser.FullName() + " successfully logged out");
                Login login = new Login();
                this.NavigationService.Navigate(login);
            }
            catch (Exception e1)
            {
                Home.HomeNotifier.ShowError(e1.Message);
            }
        }
    }
}
