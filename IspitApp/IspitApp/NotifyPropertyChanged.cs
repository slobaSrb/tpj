﻿using IspitApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ExamApp
{
    public class NotifyPropertyChanged :INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        // Atribut [CallerMemberName] omogucava da default vrednost opcionog parametra propertyName 
        // bude naziv clanice klase (metode, property-ja) koja je pozvala OnPropertyChanged metod.
        // Tako se izbegava eksplicitno pisanje naziva propertyja prilikom poziva OnPropertyChanged metoda.

        // Postoje i [CallerFilePath] i [CallerLineNumber] atributi (pogledati Programming C# 5.0 - Ian Griffiths)
        protected void OnPropertyChanged(
             [CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
    public class Props : NotifyPropertyChanged
    {
        public List<User> StudentsOnExam { get { return StudentsOnExam; } set { StudentsOnExam = value; this.OnPropertyChanged(); } }
    }
}
