﻿using IspitApp;
using IspitApp.Models;
using IspitApp.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ToastNotifications;
using ToastNotifications.Messages;
using ToastNotifications.Lifetime;
using ToastNotifications.Position;

namespace ExamApp
{
    /// <summary>
    /// Interaction logic for AllUsers.xaml
    /// </summary>
    public partial class SeeUsersForExam : Page
    {
        public List<User> StudentsOnExam { get; set; }
        public List<User> StudentsOnExamReserve { get; set; }
        public Exam ExamOnSeeUsers { get; set; }
        public TextBox SearchTextBox { get; set; }
        public SeeUsersForExam()
        {
            InitializeComponent();
            CreateSearchBar();
            this.Loaded += SeeUsersForExam_Loaded;
            this.SizeChanged += SeeUsersForExam_SizeChanged;
            //this.PropertyChanged += SeeUsers_PropertyChanged;


        }

        private void SeeUsersForExam_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            SearchTextBox.Width = e.NewSize.Width / 4;
        }

        //private void SeeUsers_PropertyChanged(object sender, PropertyChangedEventArgs e)
        //{
        //    seeUsersGrid.Children.Clear(); 
        //    CreateLayout();
        //}


        private void SeeUsersForExam_Loaded(object sender, RoutedEventArgs e)
        {
            CreateLayout();
            //StudentsOnExam.CollectionChanged += (eve, v) => CreateLayout();
        }
        private void CreateSearchBar()
        {
            Label lblTitle = new Label();
            SearchTextBox = new TextBox();
            SearchTextBox.SetBinding(TextBox.WidthProperty, new Binding("SearchBoxWidth"));
            SearchTextBox.KeyUp += SearchTextBox_KeyUp;
            //searchTextBox.Width = SearchBoxWidth;
            lblTitle.Content = new StackPanel();
            (lblTitle.Content as StackPanel).Orientation = Orientation.Horizontal;
            (lblTitle.Content as StackPanel).Children.Add(new Label() { Content = "Search by name, email, index number :" });
            (lblTitle.Content as StackPanel).Children.Add(SearchTextBox);
            (lblTitle.Content as StackPanel).Children.Add(new Label() { Content = "All Users - Admin Page" });

            Grid.SetColumn(lblTitle, 0);
            Grid.SetRow(lblTitle, 0);
            searchScroll.Content = lblTitle;
        }

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            SearchTextBox = (sender as TextBox);

            Filter();

            CreateLayout();
        }

        private void Filter()
        {
            if (StudentsOnExamReserve == null)
            {
                User[] users = new User[StudentsOnExam.Count];
                StudentsOnExam.CopyTo(users);
                StudentsOnExamReserve = users.ToList();
            }
            if (SearchTextBox.Text == "")
            {
                StudentsOnExam = StudentsOnExamReserve.Where(u => u.FirstName != "0").ToList();
            }
            else
            {
                StudentsOnExam = StudentsOnExamReserve.Where(u => u.FullName().ToLower().Contains(SearchTextBox.Text.ToLower()) || u.Email.ToLower().Contains(SearchTextBox.Text.ToLower()) || u.IndexNo.ToLower().Contains(SearchTextBox.Text.ToLower())).ToList();
            }
        }

        private void CreateLayout()
        {
            seeUsersGrid.Children.Clear();
            seeUsersGrid.ColumnDefinitions.Clear();
            seeUsersGrid.RowDefinitions.Clear();
            ColumnDefinition colIndexNo = new ColumnDefinition();
            colIndexNo.Width = new GridLength(2, GridUnitType.Star);
            ColumnDefinition colFullName = new ColumnDefinition();
            colFullName.Width = new GridLength(7, GridUnitType.Star);
            ColumnDefinition colBtnDel = new ColumnDefinition();
            colBtnDel.Width = new GridLength(1, GridUnitType.Star);
            seeUsersGrid.ColumnDefinitions.Add(colIndexNo);
            seeUsersGrid.ColumnDefinitions.Add(colFullName);
            seeUsersGrid.ColumnDefinitions.Add(colBtnDel);
            for (int i = 0; i < StudentsOnExam.Count; i++)
            {
                RowDefinition row = new RowDefinition();
                row.Height = new GridLength(100, GridUnitType.Pixel);
                seeUsersGrid.RowDefinitions.Add(row);
            }

            for (int i = 0; i < StudentsOnExam.Count; i++)
            {
                SolidColorBrush background;
                if (i % 2 == 0)
                {
                    background = new SolidColorBrush(Colors.LightBlue);
                }
                else
                {
                    background = new SolidColorBrush(Colors.White);
                }
                Border border0 = new Border() { BorderBrush = new SolidColorBrush(Colors.Black), Background = background };
                Border border1 = new Border() { BorderBrush = new SolidColorBrush(Colors.Black), Background = background };
                Border border2 = new Border() { BorderBrush = new SolidColorBrush(Colors.Black), Background = background };
                Grid.SetRow(border0, i);
                Grid.SetRow(border1, i);
                Grid.SetRow(border2, i);
                Grid.SetColumn(border0, 0);
                Grid.SetColumn(border1, 1);
                Grid.SetColumn(border2, 2);
                seeUsersGrid.Children.Add(border0);
                seeUsersGrid.Children.Add(border1);
                seeUsersGrid.Children.Add(border2);
                Label lblIndexNo = new Label() { Content = StudentsOnExam[i].IndexNo };
                Grid.SetRow(lblIndexNo, i);
                Grid.SetColumn(lblIndexNo, 0);
                seeUsersGrid.Children.Add(lblIndexNo);

                Label lblFullName = new Label() { Content = StudentsOnExam[i].FullName() };
                Grid.SetRow(lblFullName, i);
                Grid.SetColumn(lblFullName, 1);
                seeUsersGrid.Children.Add(lblFullName);

                Button btnDelete = new Button() { Content = "UnEnroll Student", Name = "UnEnroll_" + StudentsOnExam[i].UserID.ToString() + "_" + StudentsOnExam[i].FullName().Replace(" ", "_") };
                btnDelete.Click += BtnUnEnroll_Click;
                btnDelete.FontSize = 10;
                Grid.SetRow(btnDelete, i);
                Grid.SetColumn(btnDelete, 2);
                seeUsersGrid.Children.Add(btnDelete);

            }
        }

        private void BtnUnEnroll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ExamService examService = new ExamService();

                StudentsOnExamReserve = examService.UnEnrollStudentFromExam(ExamOnSeeUsers, (sender as Button));//.ToObservable();
                Filter();
                CreateLayout();
                Home.HomeNotifier.ShowSuccess("Student successfuly UnEnrolled");
            }
            catch (Exception e1)
            {
                Home.HomeNotifier.ShowError(e1.Message);
            }
        }

    }
}

