﻿using ExamApp;
using ExamApp.ChatApp.Client;
using IspitApp.Models;
using IspitApp.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using ToastNotifications;
using ToastNotifications.Lifetime;
using ToastNotifications.Messages;
using ToastNotifications.Position;

namespace IspitApp
{
    /// <summary>
    /// Interaction logic for Home.xaml
    /// </summary>
    public partial class Home : Page, INotifyPropertyChanged
    {
        //public static Dictionary<string, ClientModel> ClientModels = new Dictionary<string, ClientModel>(); 
        ExamService examService = new ExamService();
        UserService userService = new UserService();

        public static MainWindow MainWindow { get; internal set; }
        public string AppName { get { return MainWindow.AppName; } set { MainWindow.AppName = value; OnPropertyChanged("AppName"); } }

        //Props Props = new Props();
        //public User User { get; set; }

        public static User LoggedInUser { get; set; }
        public List<Exam> AllExams { get; set; }
        List<Exam> AllExamsReserve { get; set; }
        public static Token Token { get; set; }
        private Exam selectedExam;
        private static Notifier _homeNotifier = new Notifier(cfg =>
        {
            cfg.PositionProvider = new WindowPositionProvider(
                parentWindow: Application.Current.MainWindow,
                corner: Corner.TopRight,
                offsetX: 10,
                offsetY: 10);

            cfg.LifetimeSupervisor = new TimeAndCountBasedLifetimeSupervisor(
                notificationLifetime: TimeSpan.FromSeconds(3),
                maximumNotificationCount: MaximumNotificationCount.FromCount(5));
            cfg.Dispatcher = Application.Current.Dispatcher;
            }
        );

        private void ClearNotifications()
        {

        }
        public static Notifier HomeNotifier { get { return _homeNotifier; } set { _homeNotifier = value; } }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(
        [CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        private double _imageWidth;
        private double _imageHeight;
        public double ImageWidth { get { return _imageWidth; } set { _imageWidth = value; OnPropertyChanged(); } }
        public double ImageHeight { get { return _imageHeight; } set { _imageHeight = value; OnPropertyChanged(); } }

        private double _btnHeight;
        private double _btnWidth;
        public double BtnHeight { get { return _btnHeight; } set { _btnHeight = value; OnPropertyChanged(); } }
        public double BtnWidth { get { return _btnWidth; } set { _btnWidth = value; OnPropertyChanged(); } }

        public static CheckIn CheckIn { get; internal set; }
        public StackPanel StackPanel1 { get; private set; }
        public Label LabelSearch { get; private set; }
        public TextBox SearchTextBox { get; private set; }

        public Home()
        {
            InitializeComponent();
            LoadData();
            MakeSideBar();
            MakeSearch();
            CreateLayout();
            MainWindow.PropertyChanged += MainWindow_PropertyChanged;
            this.Loaded += Home_Loaded;
            this.SizeChanged += Home_SizeChanged;
            MainWindow.AppName = "Exam App - " + Home.LoggedInUser.FullName();
        }

        private void MainWindow_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("AppName"))
            {
                OnPropertyChanged("AppName");
            }
        }

        private void LoadData()
        {
            if (AllExams == null)
            {
                AllExams = examService.GetAllExamsForUser();
                //AllExams = examDataService.GetAllExamsForUser(LoggedInUser.UserID);
            }
            this.DataContext = LoggedInUser;
        }

        private void MakeSideBar()
        {

            Button btnExams = new Button();
            btnExams.Content = "Exams";
            btnExams.Click += BtnExams_Click;
            leftMenu.Children.Add(btnExams);


            Button btnLogout = new Button();
            btnLogout.Content = "Logout";
            btnLogout.Click += BtnLogout_Click;
            leftMenu.Children.Add(btnLogout);

            if (LoggedInUser.Roles.ContainsRoleWithName("Administrator"))
            {
                Button btnAllExams = new Button();
                btnAllExams.Content = "All Exams";
                btnAllExams.Click += BtnAllExams_Click;
                leftMenu.Children.Add(btnAllExams);

                Button btnAllUsers = new Button();
                btnAllUsers.Content = "All Users";
                btnAllUsers.Click += BtnAllUsers_Click;
                leftMenu.Children.Add(btnAllUsers);
            }
        }

        private void Home_Loaded(object sender, RoutedEventArgs e)
        {
            this.NavigationService.RemoveBackEntry();
        }

        private void Home_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            BtnHeight = e.NewSize.Height / 4;
            BtnWidth = e.NewSize.Width / 6;
            ImageHeight = e.NewSize.Height / 4 - e.NewSize.Height / 4 * 30 / 100;
            ImageWidth = e.NewSize.Width / 4 - e.NewSize.Width / 4 * 30 / 100;
            foreach (var child in (ExamWrapPanel).Children)
            {
                (child as Button).Width = e.NewSize.Width / 6;
                (child as Button).Height = e.NewSize.Height / 4;
                (((child as Button).Content as StackPanel).Children[0] as Image).Width  = e.NewSize.Width / 4 - e.NewSize.Width / 4 * 30 / 100;
                (((child as Button).Content as StackPanel).Children[0] as Image).Height = e.NewSize.Height / 4 - e.NewSize.Height / 4 * 30 / 100;
            }
            //((lblTitle.Content as StackPanel).Children[1] as TextBox).MinWidth = e.NewSize.Width / 5;
            foreach (var child in (leftMenu).Children)
            {
                (child as Button).Width = e.NewSize.Width / 10;
                (child as Button).Height = e.NewSize.Width / 10;

            }
            SearchTextBox.Width = e.NewSize.Width / 4;
        }

        private void CreateLayout()
        {

            ExamWrapPanel.Children.Clear();
            try
            {
                for (int i = 0; i < AllExams.Count; i++)
                {
                    Button btnExam = new Button();
                    StackPanel spPom = new StackPanel();
                    var img = new Image();
                    btnExam.Height = BtnHeight;
                    btnExam.Width = BtnWidth;
                    img.Height = ImageHeight;
                    img.Width = ImageWidth;
                    spPom.Children.Add(img);
                    var tb = new TextBlock();
                    spPom.Children.Add(tb);
                    img.Source = new BitmapImage(new Uri(AllExams[i].ExamImagePath, UriKind.Relative));
                    tb.Text = AllExams[i].ExamName;
                    btnExam.Click += BtnExam_OptionsShow;

                    btnExam.Content = spPom;
                    btnExam.DataContext = AllExams[i];
                    ExamWrapPanel.Children.Add(btnExam);
                }
            }
            catch (Exception e1)
            {
                //could implement status codes here
                Home.HomeNotifier.ShowError(e1.Message);
            }


        }

        private void MakeSearch()
        {
            SearchTextBox = new TextBox() { Name = "SearchTextBox" };
            SearchTextBox.KeyUp += SearchTextBox_KeyUp;

            lblTitle.Background = new SolidColorBrush(Colors.CornflowerBlue);
            lblTitle.HorizontalAlignment = HorizontalAlignment.Stretch;
            lblTitle.VerticalAlignment = VerticalAlignment.Stretch;
            lblTitle.HorizontalContentAlignment = HorizontalAlignment.Center;
            lblTitle.VerticalContentAlignment = VerticalAlignment.Center;
            lblTitle.Foreground = new SolidColorBrush(Colors.White);


            StackPanel1 = new StackPanel();
            lblTitle.Content = StackPanel1;
            StackPanel1.Orientation = Orientation.Horizontal;
            LabelSearch = new Label() { Content = "Search :" };
            StackPanel1.Children.Add(LabelSearch);
            StackPanel1.Children.Add(SearchTextBox);
            StackPanel1.Children.Add(new Label() { Content = LoggedInUser.FullName() + " - Exams Home" });
        }

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            SearchTextBox = (sender as TextBox);

            //if (mainPanel.Children.Count != 0) btnWidth = (mainPanel.Children[0] as Button).Width;
            //if (mainPanel.Children.Count != 0) btnHeight = (mainPanel.Children[0] as Button).Height;
            Filter();
            CreateLayout();
        }

        private void Filter()
        {
            if (AllExamsReserve == null)
            {
                Exam[] exams = new Exam[AllExams.Count];
                AllExams.CopyTo(exams);
                AllExamsReserve = exams.ToList();
            }
            if (SearchTextBox.Text == "")
            {
                AllExams = AllExamsReserve.Where(e1 => e1.ExamName != "0").ToList();
            }
            else
            {
                AllExams = AllExamsReserve.Where(e1 => e1.ExamName.ToLower().Contains(SearchTextBox.Text.ToLower())).ToList();
            }
        }


        private static void BtnExams_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnLogout_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                User loggedOutUser = userService.LogOut();
                Login login = new Login();
                Home.HomeNotifier.ShowSuccess("User " + Home.LoggedInUser.FullName() +" logged out");
                this.NavigationService.Navigate(login);
            }
            catch (Exception e1)
            {
                Home.HomeNotifier.ShowError(e1.Message);
            }
        }
        private void BtnAllExams_Click(object sender, RoutedEventArgs e)
        {
            SeeAllExams allExams = new SeeAllExams();
            this.NavigationService.Navigate(allExams);
        }
        private void BtnAllUsers_Click(object sender, RoutedEventArgs e)
        {
            SeeAllUsers allUsers = new SeeAllUsers();
            this.NavigationService.Navigate(allUsers);
        }

        private void BtnExam_OptionsShow(object sender, EventArgs e)
        {
            try
            {
                selectedExam = (sender as Button).DataContext as Exam;
                ExamRead.SelectedExam = selectedExam;
                ExamRead examRead = new ExamRead();
                //examRead.HomeNotifier = HomeNotifier;
                examRead.Exams = examService.GetAllExams();
                examRead.LoggedInUser = Home.LoggedInUser;
                this.NavigationService.Navigate(examRead);
            }
            catch (Exception e1)
            {
                Home.HomeNotifier.ShowError(e1.Message);
            }
        }

    }
}
