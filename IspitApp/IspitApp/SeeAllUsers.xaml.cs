﻿using IspitApp;
using IspitApp.Models;
using IspitApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ToastNotifications;
using ToastNotifications.Messages;
using ToastNotifications.Lifetime;
using ToastNotifications.Position;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ExamApp
{
    /// <summary>
    /// Interaction logic for SeeAllUsers.xaml
    /// </summary>
    public partial class SeeAllUsers : Page, INotifyPropertyChanged
    {
        public List<User> AllUsers { get; set; }
        public UserService usersService = new UserService();
        public TextBox SearchTextBox { get; set; }
        public List<User> AllUsersReserve { get; set; }
        public Notifier HomeNotifier { get; set; }
        public SeeAllUsers()
        {
            InitializeComponent();
            LoadData();
            this.MinHeight = 400;
            this.MinWidth = 600;
            CreateSearchBar();
            CreateLayout();
            this.SizeChanged += SeeAllUsers_SizeChanged1;
            //this.Loaded += SeeAllUsers_Loaded;
            //this.SizeChanged += SeeAllUsers_SizeChanged;
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(
     [CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        private void CreateSearchBar()
        {
            Label lblTitle = new Label();
            SearchTextBox = new TextBox();
            SearchTextBox.SetBinding(TextBox.WidthProperty, "SearchBoxWidth");
            SearchTextBox.KeyUp += SearchTextBox_KeyUp;
            lblTitle.Content = new StackPanel();
            (lblTitle.Content as StackPanel).Orientation = Orientation.Horizontal;
            (lblTitle.Content as StackPanel).Children.Add(new Label() { Content = "Search by name, email, index number :" });
            (lblTitle.Content as StackPanel).Children.Add(SearchTextBox);
            (lblTitle.Content as StackPanel).Children.Add(new Label() { Content = "All Users - Admin Page" });

            Grid.SetColumn(lblTitle, 0);
            Grid.SetRow(lblTitle, 0);
            searchScroll.Content = lblTitle;
        }

        private void SeeAllUsers_SizeChanged1(object sender, SizeChangedEventArgs e)
        {
            SearchTextBox.Width = e.NewSize.Width / 4;
        }

        private void LoadData()
        {
            try
            {
                AllUsers = usersService.GetAllUsers();
            }
            catch (Exception e)
            {
                Home.HomeNotifier.ShowError(e.Message);
            }
        }

        private void CreateLayout()
        {
            seeAllUsers.Children.Clear();
            seeAllUsers.ColumnDefinitions.Clear();
            seeAllUsers.RowDefinitions.Clear();
            ColumnDefinition colIndexNo = new ColumnDefinition();
            colIndexNo.Width = new GridLength(2, GridUnitType.Star);
            ColumnDefinition colFullName = new ColumnDefinition();
            colFullName.Width = new GridLength(5, GridUnitType.Star);
            ColumnDefinition colBtnApprove = new ColumnDefinition();
            colBtnApprove.Width = new GridLength(1, GridUnitType.Star);
            ColumnDefinition colBtnEdit = new ColumnDefinition();
            colBtnEdit.Width = new GridLength(1, GridUnitType.Star);
            ColumnDefinition colBtnDelete = new ColumnDefinition();
            colBtnApprove.Width = new GridLength(1, GridUnitType.Star);
            seeAllUsers.ColumnDefinitions.Add(colIndexNo);
            seeAllUsers.ColumnDefinitions.Add(colFullName);
            seeAllUsers.ColumnDefinitions.Add(colBtnApprove);
            seeAllUsers.ColumnDefinitions.Add(colBtnEdit);
            seeAllUsers.ColumnDefinitions.Add(colBtnDelete);
            

            for (int i = 0; i < AllUsers.Count; i++)
            {
                RowDefinition row = new RowDefinition();
                row.Height = new GridLength(100, GridUnitType.Pixel);
                seeAllUsers.RowDefinitions.Add(row);
            }

            for (int i = 0; i < AllUsers.Count; i++)
            {
                SolidColorBrush background;
                if (i % 2 == 0)
                {
                    background = new SolidColorBrush(Colors.LightBlue);
                }
                else
                {
                    background = new SolidColorBrush(Colors.White);
                }
                Border border0 = new Border() { BorderBrush = new SolidColorBrush(Colors.Black), Background = background };
                Border border1 = new Border() { BorderBrush = new SolidColorBrush(Colors.Black), Background = background };
                Border border2 = new Border() { BorderBrush = new SolidColorBrush(Colors.Black), Background = background };
                Border border3 = new Border() { BorderBrush = new SolidColorBrush(Colors.Black), Background = background };
                Border border4 = new Border() { BorderBrush = new SolidColorBrush(Colors.Black), Background = background };
                Grid.SetRow(border0, i);
                Grid.SetRow(border1, i);
                Grid.SetRow(border2, i);
                Grid.SetRow(border3, i);
                Grid.SetRow(border4, i);
                Grid.SetColumn(border0, 0);
                Grid.SetColumn(border1, 1);
                Grid.SetColumn(border2, 2);
                Grid.SetColumn(border3, 3);
                Grid.SetColumn(border4, 4);
                seeAllUsers.Children.Add(border0);
                seeAllUsers.Children.Add(border1);
                seeAllUsers.Children.Add(border2);
                seeAllUsers.Children.Add(border3);
                seeAllUsers.Children.Add(border4);
                Label lblIndexNo = new Label() { Content = AllUsers[i].IndexNo };
                Grid.SetRow(lblIndexNo, i);
                Grid.SetColumn(lblIndexNo, 0);
                seeAllUsers.Children.Add(lblIndexNo);

                Label lblFullNameAndData = new Label() { Content = AllUsers[i].FullName() + "\n" + AllUsers[i].Email };
                Grid.SetRow(lblFullNameAndData, i);
                Grid.SetColumn(lblFullNameAndData, 1);
                seeAllUsers.Children.Add(lblFullNameAndData);
                if (!AllUsers[i].Approved)
                {
                    Button btnApprove = new Button() { Background = new SolidColorBrush(Colors.LightGreen), Content = "Approve User", Name = "Approve_" + AllUsers[i].UserID.ToString() + "_" + AllUsers[i].FullName().Replace(" ", "_") };
                    btnApprove.Click += BtnApprove_Click;
                    btnApprove.FontSize = 10;
                    Grid.SetRow(btnApprove, i);
                    Grid.SetColumn(btnApprove, 2);
                    seeAllUsers.Children.Add(btnApprove);
                }
                else
                {
                    Button btnForbid = new Button() { Background = new SolidColorBrush(Colors.Red), Content = "Forbid User", Name = "Forbid_" + AllUsers[i].UserID.ToString() + "_" + AllUsers[i].FullName().Replace(" ", "_") };
                    btnForbid.Click += BtnForbid_Click; ;
                    btnForbid.FontSize = 10;
                    Grid.SetRow(btnForbid, i);
                    Grid.SetColumn(btnForbid, 2);
                    seeAllUsers.Children.Add(btnForbid);
                }
                Button btnEdit = new Button() { Background = new SolidColorBrush(Colors.DeepSkyBlue), Content = "Edit User", Name = "Edit_" + AllUsers[i].UserID.ToString() + "_" + AllUsers[i].FullName().Replace(" ", "_") };
                btnEdit.Click += BtnEdit_Click; ;
                btnEdit.FontSize = 10;
                Grid.SetRow(btnEdit, i);
                Grid.SetColumn(btnEdit, 3);
                seeAllUsers.Children.Add(btnEdit);

                Button btnDelete = new Button() { Background = new SolidColorBrush(Colors.PaleVioletRed), Content = "Delete User", Name = "Delete_" + AllUsers[i].UserID.ToString() + "_" + AllUsers[i].FullName().Replace(" ", "_") };
                btnDelete.Click += BtnDelete_Click;
                btnDelete.FontSize = 10;
                Grid.SetRow(btnDelete, i);
                Grid.SetColumn(btnDelete, 4);
                seeAllUsers.Children.Add(btnDelete);

            }

        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            SearchTextBox = (sender as TextBox);

            Filter();

            CreateLayout();
            //if (mainPanel.Children.Count != 0) btnWidth = (mainPanel.Children[0] as Button).Width;
            //if (mainPanel.Children.Count != 0) btnHeight = (mainPanel.Children[0] as Button).Height;

        }

        private void Filter()
        {
            if (AllUsersReserve == null)
            {
                User[] users = new User[AllUsers.Count];
                AllUsers.CopyTo(users);
                AllUsersReserve = users.ToList();
            }
            if (SearchTextBox.Text == "")
            {
                AllUsers = AllUsersReserve.Where(u => u.FirstName != "0").ToList();
            }
            else
            {
                AllUsers = AllUsersReserve.Where(u => u.FullName().ToLower().Contains(SearchTextBox.Text.ToLower()) || u.Email.ToLower().Contains(SearchTextBox.Text.ToLower()) || u.IndexNo.ToLower().Contains(SearchTextBox.Text.ToLower())).ToList();
            }
        }


        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void BtnApprove_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int userID = int.Parse((sender as Button).Name.Split('_')[1]);
                AllUsers = usersService.ApproveUser(AllUsers.SingleOrDefault(u => u.UserID == userID));
                AllUsersReserve = AllUsers;
                Filter();
                CreateLayout();
            }
            catch(Exception e1)
            {
                Home.HomeNotifier.ShowError("User not approved");
            }
        }

        private void BtnForbid_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int userID = int.Parse((sender as Button).Name.Split('_')[1]);
                AllUsers = usersService.ForbidUser(AllUsers.SingleOrDefault(u => u.UserID == userID));
                AllUsersReserve = AllUsers;
                Filter();
                CreateLayout();
            }
            catch (Exception e1)
            {
                Home.HomeNotifier.ShowError("User not forbiden");
            }
        }

    }
}