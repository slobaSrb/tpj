﻿using ExamApp;
using IspitApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ToastNotifications;
using ToastNotifications.Lifetime;
using ToastNotifications.Position;
using ToastNotifications.Messages;
using System.Runtime.CompilerServices;

namespace IspitApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : NavigationWindow, INotifyPropertyChanged
    {
        public string _appName { get; set; }
        public string AppName { get { return _appName; } set { _appName = value; OnPropertyChanged("AppName"); } }
        

        public MainWindow()
        {
            InitializeComponent();
            this.MinHeight = 500;
            this.MinWidth = 800;
            this.SizeChanged += MainWindow_SizeChanged;
            this.DataContext = this;
            //this.PropertyChanged += MainWindow_PropertyChanged;
            AppName = "Exam App";
            Home.MainWindow = this;
            Login.MainWindow = this;
            FileIOPermission f2 = new FileIOPermission(FileIOPermissionAccess.Read, "C:\\fax");
            f2.AddPathList(FileIOPermissionAccess.Write | FileIOPermissionAccess.Read, Environment.CurrentDirectory);
            try
            {
                f2.Demand();
            }
            catch (SecurityException s)
            {
                Console.WriteLine(s.Message);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this,new PropertyChangedEventArgs(propertyName));
        }
        private void MainWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
        }
    }
}
