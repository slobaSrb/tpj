﻿using IspitApp;
using System;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ToastNotifications.Messages;

namespace ExamApp.ChatApp.Server
{
    public class ChatServer
    {
        public static Hashtable ClientList = new Hashtable();
        //Stop:
        public static void StartServer()
        {
            var serverSocket = new TcpListener(IPAddress.Any, 8888 + ExamRead.SelectedExam.ExamID);
            var clientSocket = default(TcpClient);
            try { 
            //chat server started
            Console.WriteLine("Chat server started...");
            int counter = 0;
            serverSocket.Start();
            while (true)
            {
                counter += 1;
                //stopping the execution and waits for somebody to connect
                clientSocket = serverSocket.AcceptTcpClient();
                //Connected and send us data
                string dataFromClient = GetStringFromStream(clientSocket);
                if (!ClientList.ContainsKey(dataFromClient)){
                    ClientList.Add(dataFromClient, clientSocket);
                }
                Broadcast(dataFromClient + " joined ", dataFromClient, false);
                Console.WriteLine(dataFromClient + " jointed chat room.");//Someone joined
                var client = new HandleClient();
                client.StartClient(clientSocket, dataFromClient, ClientList);

            }
            } catch(Exception ex)
            {
                Home.HomeNotifier.ShowError("Chat Server Stopped");
                Stop(serverSocket);
            }
        }

        private static void Stop(TcpListener serverSocket)
        {
            serverSocket.Stop();
        }

        public static void Broadcast(string msg, string uname, bool flag)
        {
            foreach (DictionaryEntry item in ClientList)
            {
                var broadcastSocket = ((TcpClient)item.Value);
                NetworkStream broadcastStream = broadcastSocket.GetStream();
                byte[] broadcastBytes = flag ? Encoding.ASCII.GetBytes(uname + " says: " + msg) : Encoding.ASCII.GetBytes(msg);
                broadcastStream.Write(broadcastBytes, 0, broadcastBytes.Length);
                broadcastStream.Flush();
            }
        }

        public static string GetStringFromStream(TcpClient clientSocket)
        {
            var bytesFrom = new byte[65536];
            NetworkStream networkStream = clientSocket.GetStream();
            networkStream.Read(bytesFrom, 0, clientSocket.ReceiveBufferSize);
            string dataFromClient = Encoding.ASCII.GetString(bytesFrom);
            return dataFromClient.Substring(0, dataFromClient.IndexOf("\0"));
        }

    }

    public class HandleClient
    {
        private TcpClient _clientSocket;
        private string _clientNumber;
        private Hashtable _clientList;

        public void StartClient(TcpClient clientSocket, string clientNumber, Hashtable clientList)
        {
            _clientList = clientList;
            _clientNumber = clientNumber;
            _clientSocket = clientSocket;
            var thread = new Thread(DoChat);
            thread.IsBackground = true;
            thread.Start();
        }

        private void DoChat()
        {
            var bytesFrom = new byte[8192];
            int requestCount = 0;
            while (true)
            {
                try
                {
                    requestCount += 1;
                    string dataFromClient = ChatServer.GetStringFromStream(_clientSocket);
                    Console.WriteLine("From Client - " + _clientNumber + ": " + dataFromClient);
                    ChatServer.Broadcast(dataFromClient, _clientNumber, true);
                }
                catch (Exception ex)
                {
                    Home.HomeNotifier.ShowError(ex.Message);
                }
            }
        }
    }
}
