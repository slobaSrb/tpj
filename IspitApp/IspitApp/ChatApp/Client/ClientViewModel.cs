﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ExamApp.ChatApp.Client
{
    public class ClientViewModel : INotifyPropertyChanged
    {

        #region Props
        private ClientModel _clientModel;
        public string Message {
            get { return _clientModel.CurrentMessage; }
            set { 
                _clientModel.CurrentMessage = value;
                OnPropertyChanged("Message");
            }
        }

        public string MessageBoard
        {
            get { return _clientModel.MessageBoard; }
            set
            {
                _clientModel.MessageBoard = value;
                OnPropertyChanged("MessageBoard");
            }
        }


        public DelegateCommand SendCommand { get; set; }
        public DelegateCommand ConnectCommand { get; set; }
        #endregion

        public ClientViewModel()
        {
            _clientModel = new ClientModel();
            _clientModel.PropertyChanged += ClientModelChanged;
            SendCommand = new DelegateCommand(a => _clientModel.Send(), p => _clientModel.Connected);
            ConnectCommand = new DelegateCommand(a => _clientModel.Connect(), p => !_clientModel.Connected);
        }



        private void ClientModelChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("Connected"))
            {
                OnPropertyChanged("Connected");
                ConnectCommand.RaiseCanExecuteChanged();
                SendCommand.RaiseCanExecuteChanged();
            }
            else if (e.PropertyName.Equals("MessageBoard"))
            {
                OnPropertyChanged("MessageBoard");
            }
        }


        #region INPCH

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
