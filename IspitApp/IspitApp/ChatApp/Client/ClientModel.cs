﻿using IspitApp;
using System;
using System.ComponentModel;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using ToastNotifications.Messages;

namespace ExamApp.ChatApp.Client
{
    public class ClientModel : INotifyPropertyChanged
    {
        private ClientStringSocket _socket;

        #region Props
        private string _messageBoard;

        public string MessageBoard
        {
            get { return _messageBoard; }
            set
            {
                _messageBoard = value;
                OnPropertyChanged("MessageBoard");
            }
        }
        private string _currentMessage;
        public string CurrentMessage
        {
            get { return _currentMessage; }
            set
            {
                _currentMessage = value;
                OnPropertyChanged("CurrentMessage");
            }
        }

        public bool Connected
        {
            get { return _socket != null; /*&& _socket.Connected;*/ }
        }

        #endregion



        public void Connect()
        {
            try
            {
                _socket = new ClientStringSocket("127.0.0.1", 8000 + ExamRead.SelectedExam.ExamID);
                OnPropertyChanged("Connected");
                _messageBoard = "Welcome: ";// + _currentMessage;
                _currentMessage = Home.LoggedInUser.FullName();
                Send();
                var thread = new Thread(GetMessage);
                thread.IsBackground = true;
                thread.Start();
            }
            catch (Exception ex)
            {
                Home.HomeNotifier.ShowError(ex.Message);
            }
        }

        private void GetMessage()
        {
            while (true)
            {
                string msg = _socket.ReadString();
                if(msg == "Chat server not working" && Home.LoggedInUser.Roles.ContainsRoleWithName("Professor"))
                {
                    _socket.GetStream().Close();
                }
                MessageBoard += "\r\n" + msg;
            }
        }

        public void Send()
        {
            _socket.WriteString(_currentMessage);
        }




        #region INPCH

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

    }
}
