﻿using System.Net.Sockets;
using System.Text;

namespace ExamApp.ChatApp.Client
{
    public class ClientStringSocket : TcpClient
    {
        private NetworkStream _stream;
        public ClientStringSocket(string hostname, int port) : base(hostname, port)
        {
            _stream = GetStream();
        }

        public string ReadString()
        {
            try { 
            var bytes = new byte[65536];
            _stream.Read(bytes, 0, ReceiveBufferSize);
            string msg = Encoding.ASCII.GetString(bytes);
            return msg.Substring(0, msg.IndexOf("\0"));
            }
            catch
            {
                return "Chat server not working";
            }
            }

        public void WriteString(string msg)
        {
            msg += "\0";
            byte[] bytes = Encoding.ASCII.GetBytes(msg);
            _stream.Write(bytes, 0, bytes.Length);
            _stream.Flush();
        }
    }
}
