﻿using IspitApp;
using IspitApp.Models;
using IspitApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ToastNotifications;
using ToastNotifications.Lifetime;
using ToastNotifications.Messages;
using ToastNotifications.Position;
namespace ExamApp
{
    /// <summary>
    /// Interaction logic for SeeAllExams.xaml
    /// </summary>
    public partial class SeeAllExams : Page
    {
        public List<Exam> AllExams { get; set; }
        public ExamService examsService = new ExamService();
        public TextBox SearchTextBox { get; set; }
        public List<Exam> AllExamsReserve { get; set; }
        public SeeAllExams()
        {
            InitializeComponent();
            LoadData();
            this.MinHeight = 400;
            this.MinWidth = 600;
            CreateSearchBar();
            CreateHeader();
            CreateLayout();
            this.SizeChanged += SeeAllExams_SizeChanged; ;
        }

        private void CreateHeader()
        {
            header.Children.Clear();
            header.ColumnDefinitions.Clear();
            header.RowDefinitions.Clear();

            ColumnDefinition yearOfStudies = new ColumnDefinition();
            yearOfStudies.Width = new GridLength(1, GridUnitType.Star);
            ColumnDefinition semester = new ColumnDefinition();
            semester.Width = new GridLength(1, GridUnitType.Star);
            ColumnDefinition lvlOfStudies = new ColumnDefinition();
            lvlOfStudies.Width = new GridLength(2, GridUnitType.Star);
            ColumnDefinition colExamNameAndDeparts = new ColumnDefinition();
            colExamNameAndDeparts.Width = new GridLength(4, GridUnitType.Star);
            ColumnDefinition colBtnEditExam = new ColumnDefinition();
            colBtnEditExam.Width = new GridLength(1, GridUnitType.Star);
            ColumnDefinition colBtnDeleteExam = new ColumnDefinition();
            colBtnDeleteExam.Width = new GridLength(1, GridUnitType.Star);
            header.ColumnDefinitions.Add(yearOfStudies);
            header.ColumnDefinitions.Add(semester);
            header.ColumnDefinitions.Add(lvlOfStudies);
            header.ColumnDefinitions.Add(colExamNameAndDeparts);
            header.ColumnDefinitions.Add(colBtnEditExam);
            header.ColumnDefinitions.Add(colBtnDeleteExam);

            RowDefinition row = new RowDefinition();
            row.Height = new GridLength(100, GridUnitType.Star);
            header.RowDefinitions.Add(row);


            SolidColorBrush background = new SolidColorBrush(Colors.LightGray);

            Border border0 = new Border() { BorderBrush = new SolidColorBrush(Colors.Black), Background = background };

            Grid.SetRow(border0, 0);
            Grid.SetColumn(border0, 0);
            Grid.SetColumnSpan(border0, 6);
            header.Children.Add(border0);

            Label lblYearOfStudies = new Label() { Content = "Year Of\nStudies" };
            Grid.SetRow(lblYearOfStudies, 0);
            Grid.SetColumn(lblYearOfStudies, 0);
            header.Children.Add(lblYearOfStudies);

            Label lblSemester = new Label() { Content = "Semester" };
            Grid.SetRow(lblSemester, 0);
            Grid.SetColumn(lblSemester, 1);
            header.Children.Add(lblSemester);


            Label lblLvlOfStudies = new Label() { Content = "Level Of\nStudies" };
            Grid.SetRow(lblLvlOfStudies, 0);
            Grid.SetColumn(lblLvlOfStudies, 2);
            header.Children.Add(lblLvlOfStudies);


            Label lblExamNameAndDeparts = new Label() { Content = "Exam Name\nand Departments" };
            Grid.SetRow(lblExamNameAndDeparts, 0);
            Grid.SetColumn(lblExamNameAndDeparts, 3);
            header.Children.Add(lblExamNameAndDeparts);

            Button btnAddExam = new Button() { Background = new SolidColorBrush(Colors.Aqua), Content = "Add New Exam" };
            btnAddExam.Click += BtnAddExam_Click;
            btnAddExam.FontSize = 10;
            Grid.SetRow(btnAddExam, 0);
            Grid.SetColumn(btnAddExam, 4);
            header.Children.Add(btnAddExam);


        }

        private void CreateLayout()
        {
            seeAllExams.Children.Clear();
            seeAllExams.ColumnDefinitions.Clear();
            seeAllExams.RowDefinitions.Clear();

            ColumnDefinition yearOfStudies = new ColumnDefinition();
            yearOfStudies.Width = new GridLength(1, GridUnitType.Star);
            ColumnDefinition semester = new ColumnDefinition();
            semester.Width = new GridLength(1, GridUnitType.Star);
            ColumnDefinition lvlOfStudies = new ColumnDefinition();
            lvlOfStudies.Width = new GridLength(2, GridUnitType.Star);
            ColumnDefinition colExamNameAndDeparts = new ColumnDefinition();
            colExamNameAndDeparts.Width = new GridLength(4, GridUnitType.Star);
            ColumnDefinition colBtnEditExam = new ColumnDefinition();
            colBtnEditExam.Width = new GridLength(1, GridUnitType.Star);
            ColumnDefinition colBtnDeleteExam = new ColumnDefinition();
            colBtnDeleteExam.Width = new GridLength(1, GridUnitType.Star);
            seeAllExams.ColumnDefinitions.Add(yearOfStudies);
            seeAllExams.ColumnDefinitions.Add(semester);
            seeAllExams.ColumnDefinitions.Add(lvlOfStudies);
            seeAllExams.ColumnDefinitions.Add(colExamNameAndDeparts);
            seeAllExams.ColumnDefinitions.Add(colBtnEditExam);
            seeAllExams.ColumnDefinitions.Add(colBtnDeleteExam);

            for (int i = 0; i < AllExams.Count; i++)
            {
                RowDefinition row = new RowDefinition();
                row.Height = new GridLength(100, GridUnitType.Pixel);
                seeAllExams.RowDefinitions.Add(row);
            }

            for (int i = 0; i < AllExams.Count; i++)
            {
                SolidColorBrush background;
                if (i % 2 == 0)
                {
                    background = new SolidColorBrush(Colors.LightBlue);
                }
                else
                {
                    background = new SolidColorBrush(Colors.White);
                }
                Border border0 = new Border() { BorderBrush = new SolidColorBrush(Colors.Black), Background = background };
                Border border1 = new Border() { BorderBrush = new SolidColorBrush(Colors.Black), Background = background };
                Border border2 = new Border() { BorderBrush = new SolidColorBrush(Colors.Black), Background = background };
                Border border3 = new Border() { BorderBrush = new SolidColorBrush(Colors.Black), Background = background };
                Border border4 = new Border() { BorderBrush = new SolidColorBrush(Colors.Black), Background = background };
                Border border5 = new Border() { BorderBrush = new SolidColorBrush(Colors.Black), Background = background };
                Border border6 = new Border() { BorderBrush = new SolidColorBrush(Colors.Black), Background = background };
                Grid.SetRow(border0, i);
                Grid.SetRow(border1, i);
                Grid.SetRow(border2, i);
                Grid.SetRow(border3, i);
                Grid.SetRow(border4, i);
                Grid.SetRow(border5, i);
                Grid.SetRow(border6, i);
                Grid.SetColumn(border0, 0);
                Grid.SetColumn(border1, 1);
                Grid.SetColumn(border2, 2);
                Grid.SetColumn(border3, 3);
                Grid.SetColumn(border4, 4);
                Grid.SetColumn(border5, 3);
                Grid.SetColumn(border6, 4);
                seeAllExams.Children.Add(border0);
                seeAllExams.Children.Add(border1);
                seeAllExams.Children.Add(border2);
                seeAllExams.Children.Add(border3);
                seeAllExams.Children.Add(border4);
                seeAllExams.Children.Add(border5);
                seeAllExams.Children.Add(border6);

                Label lblYearOfStudies = new Label() { Content = AllExams[i].YearOfStudies };
                Grid.SetRow(lblYearOfStudies, i);
                Grid.SetColumn(lblYearOfStudies, 0);
                seeAllExams.Children.Add(lblYearOfStudies);

                Label lblSemester = new Label() { Content = AllExams[i].Semester };
                Grid.SetRow(lblSemester, i);
                Grid.SetColumn(lblSemester, 1);
                seeAllExams.Children.Add(lblSemester);


                Label lblLvlOfStudies = new Label() { Content = AllExams[i].LevelOfStudies };
                Grid.SetRow(lblLvlOfStudies, i);
                Grid.SetColumn(lblLvlOfStudies, 2);
                seeAllExams.Children.Add(lblLvlOfStudies);


                Label lblExamNameAndDeparts = new Label() { Content = AllExams[i].ExamName + "\n" + AllExams[i].Departments.ToStringDeps() };
                Grid.SetRow(lblExamNameAndDeparts, i);
                Grid.SetColumn(lblExamNameAndDeparts, 3);
                seeAllExams.Children.Add(lblExamNameAndDeparts);




                Button btnEditExam = new Button() { Background = new SolidColorBrush(Colors.DeepSkyBlue), Content = "Edit Exam", Name = "Edit_" + AllExams[i].ExamID.ToString() + "_" + AllExams[i].ExamName.Replace(" ", "_") };
                btnEditExam.Click += BtnEditExam_Click;
                btnEditExam.FontSize = 10;
                Grid.SetRow(btnEditExam, i);
                Grid.SetColumn(btnEditExam, 4);
                seeAllExams.Children.Add(btnEditExam);

                Button btnDeleteExam = new Button() { Background = new SolidColorBrush(Colors.PaleVioletRed), Content = "Delete Exam", Name = "Delete_" + AllExams[i].ExamID.ToString() + "_" + AllExams[i].ExamName.Replace(" ", "_") };
                btnDeleteExam.Click += BtnDeleteExam_Click;
                btnDeleteExam.FontSize = 10;
                Grid.SetRow(btnDeleteExam, i);
                Grid.SetColumn(btnDeleteExam, 5);
                seeAllExams.Children.Add(btnDeleteExam);


                

            }

        }

        private void BtnDeleteExam_Click(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void BtnEditExam_Click(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void BtnAddExam_Click(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void SeeAllExams_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            SearchTextBox.Width = e.NewSize.Width / 4;
        }

        private void CreateSearchBar()
        {
            Label lblTitle = new Label();
            SearchTextBox = new TextBox();
            SearchTextBox.SetBinding(TextBox.WidthProperty, "SearchBoxWidth");
            SearchTextBox.KeyUp += SearchTextBox_KeyUp;
            lblTitle.Content = new StackPanel();
            (lblTitle.Content as StackPanel).Orientation = Orientation.Horizontal;
            (lblTitle.Content as StackPanel).Children.Add(new Label() { Content = "Search by Exam Name, Level Of Studies, Department, Year Of Studies :" });
            (lblTitle.Content as StackPanel).Children.Add(SearchTextBox);
            (lblTitle.Content as StackPanel).Children.Add(new Label() { Content = "All Exams - Admin Page" });

            Grid.SetColumn(lblTitle, 0);
            Grid.SetRow(lblTitle, 0);
            searchExams.Content = lblTitle;
        }

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            SearchTextBox = (sender as TextBox);

            Filter();

            CreateLayout();
        }

        private void Filter()
        {
            if (AllExamsReserve == null)
            {
                Exam[] exams = new Exam[AllExams.Count];
                AllExams.CopyTo(exams);
                AllExamsReserve = exams.ToList();
            }
            if (SearchTextBox.Text == "")
            {
                AllExams = AllExamsReserve.Where(e => e.ExamName != "0").ToList();
            }
            else
            {
                AllExams = AllExamsReserve.Where(e => e.ExamName.ToLower().Contains(SearchTextBox.Text.ToLower()) || e.LevelOfStudies.ToLower().Contains(SearchTextBox.Text.ToLower()) || e.Departments.ContainsDepartmentWithName(SearchTextBox.Text.ToLower()) || e.YearOfStudies.ToString() == SearchTextBox.Text).ToList();
            }
        }

        private void LoadData()
        {
            try
            {
                AllExams = examsService.GetAllExams();
            }
            catch (Exception e)
            {
                Home.HomeNotifier.ShowError(e.Message);
            }
        }
    }
}
