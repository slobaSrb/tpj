﻿using ExamApp.ChatApp.Client;
using IspitApp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ExamApp
{
    /// <summary>
    /// Interaction logic for Chat.xaml
    /// </summary>
    /// 
    public partial class ChatPage : Page, INotifyPropertyChanged
    {
        public static Dictionary<string, ClientViewModel> clientViewModel = new Dictionary<string, ClientViewModel>();
        public ChatPage()
        {
            InitializeComponent();
            this.SizeChanged += ChatPage_SizeChanged;
            if (!clientViewModel.ContainsKey(ExamRead.SelectedExam.ExamID.ToString()))
            {
                this.DataContext = new ClientViewModel();
                clientViewModel.Add(ExamRead.SelectedExam.ExamID.ToString(), DataContext as ClientViewModel);
                clientViewModel[ExamRead.SelectedExam.ExamID.ToString()].Message = "";
            }
            else
            {
                clientViewModel[ExamRead.SelectedExam.ExamID.ToString()].Message = "";
                this.DataContext = clientViewModel[ExamRead.SelectedExam.ExamID.ToString()];
            }
        }




        #region INPCH

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        private void ChatPage_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            txtMessage.Width = e.NewSize.Width * 90 / 100;
            txtMessage.Height = e.NewSize.Height * 17 / 100;
            txtMessageBoard.Width = e.NewSize.Width;
            txtMessageBoard.Height = e.NewSize.Height * 77 / 100;
            lblMsg.VerticalAlignment = VerticalAlignment.Bottom;
            lblMsg.HorizontalAlignment = HorizontalAlignment.Left;
            btnSend.VerticalAlignment = VerticalAlignment.Bottom;
            btnSend.HorizontalAlignment = HorizontalAlignment.Left;
            btnConnect.VerticalAlignment = VerticalAlignment.Bottom;
            btnConnect.HorizontalAlignment = HorizontalAlignment.Left;
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }

        private void btnSend_Click(object sender, RoutedEventArgs e)
        {
            txtMessage.Text = "";
        }
    }
}
