﻿using IspitApp;
using IspitApp.Models;
using IspitApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ToastNotifications;
using ToastNotifications.Messages;
using ToastNotifications.Lifetime;
using ToastNotifications.Position;
using System.Text.RegularExpressions;

namespace ExamApp
{
    /// <summary>
    /// Interaction logic for Registration.xaml
    /// </summary>
    public partial class Registration : Page
    {
        UserService userService = new UserService();

        static bool validFirstName = true;
        static bool validLastName = true;
        static bool validPassword = true;
        static bool validEmail = true;
        static bool validConfPassword = true;

        static bool matchPass = true;
        public Registration()
        {
            InitializeComponent();
            btnBackToLogin.Click += BtnBackToLogin_Click;
            btnRegister.Click += BtnRegister_Click;
            TxtConfPassword.KeyUp += TxtConfPassword_KeyUp;
        }

        private void TxtConfPassword_KeyUp(object sender, KeyEventArgs e)
        {
            matchPass = true;
            if (TxtPassword.Password != TxtConfPassword.Password)
            {
                matchPass = false;
                return;
            }
            Login.LoginNotifier.ShowSuccess("Passwords match");
        }

        private void BtnRegister_Click(object sender, RoutedEventArgs e)
        {
            validFirstName = true;
            validLastName = true;
            validPassword = true;
            validConfPassword = true;
            validEmail = true;
            if (TxtFirstName.Text == "")
            {
                Login.LoginNotifier.ShowWarning("First Name REQUIRED");
                validFirstName = false;
            }
            if (TxtLastName.Text == "")
            {
                Login.LoginNotifier.ShowWarning("Last Name REQUIRED");
                validLastName = false;
            }
            if (TxtEmail.Text == "")
            {
                Login.LoginNotifier.ShowWarning("Email REQUIRED");
                validEmail = false;
            }
            if (TxtPassword.Password == "")
            {
                Login.LoginNotifier.ShowWarning("Password REQUIRED");
                validPassword = false;
            }
            if (TxtConfPassword.Password == "")
            {
                Login.LoginNotifier.ShowWarning("Confirm Password REQUIRED");
                validConfPassword = false;
            }

            if (validFirstName && TxtFirstName.Text.Length < 3)
            {
                Login.LoginNotifier.ShowWarning("First Name too short");
                validFirstName = false;
            }
            if (validLastName && TxtLastName.Text.Length < 3)
            {
                Login.LoginNotifier.ShowWarning("Last Name too short");
                validLastName = false;
            }
            if (validEmail && !Regex.IsMatch(TxtEmail.Text, "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"))
            {
                Login.LoginNotifier.ShowWarning("Email not valid");
                validEmail = false;
            }
            if (validPassword && TxtPassword.Password.Length < 5)
            {
                Login.LoginNotifier.ShowWarning("Password length needs to be 5");
                validPassword = false;
            }

            if(validPassword && !matchPass)
            {
                Login.LoginNotifier.ShowWarning("Passwords don't match");
            }
            if (matchPass && validFirstName && validLastName && validEmail && validPassword && validConfPassword)
                try
                {
                    User regUser = new User();
                    regUser.FirstName = TxtFirstName.Text;
                    regUser.LastName = TxtLastName.Text;
                    regUser.IndexNo = TxtIndexNo.Text;
                    regUser.Password = Base64Encode(TxtPassword.Password);
                    regUser.Email = TxtEmail.Text;

                    var response = userService.RegisterUser(regUser);

                    Login.LoginNotifier.ShowSuccess(regUser.FullName() + " successfully registered to the database");
                }
                catch (Exception e3)
                {
                    Login.LoginNotifier.ShowError(e3.Message);
                }
            //if (TxtFirstName.Text.Trim() == "") && txtname.Text.Trim() != "" && txtpassword.Password.Trim() != "")
            //{
            //    TxtFirstName
            //}
            //    userService.
        }

        private void BtnBackToLogin_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.GoBack();
        }

        private static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }


        private void button1_Click(object sender, RoutedEventArgs e)
        {
            //if (txtemail.Text.Trim() != "" && txtname.Text.Trim() != "" && txtpassword.Password.Trim() != "")
            //{
            //    try
            //    {
            //        if (con.State == System.Data.ConnectionState.Closed)
            //        {
            //            con.Open();
            //        }
            //        SqlCommand cmd = new SqlCommand("proc_register", con);
            //        cmd.CommandType = System.Data.CommandType.StoredProcedure;
            //        cmd.Parameters.AddWithValue("@email", txtemail.Text.Trim());
            //        cmd.Parameters.AddWithValue("@name", txtname.Text.Trim());
            //        cmd.Parameters.AddWithValue("@password", txtpassword.Password.Trim());
            //        int i = cmd.ExecuteNonQuery();
            //        if (i > 0)
            //        {
            //            MessageBox.Show("Registration Successfull.");
            //            Login log = new Login();
            //            log.Show();
            //            this.Hide();
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        throw ex;
            //    }
            //    finally
            //    {
            //        con.Close();
            //    }
            //}
            //else
            //{
            //    MessageBox.Show("Please fill all details");
            //}
        }

    }
}
