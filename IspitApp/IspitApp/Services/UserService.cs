﻿using IspitApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ToastNotifications;
using ToastNotifications.Messages;
using ToastNotifications.Lifetime;
using ToastNotifications.Position;

namespace IspitApp.Services
{
    public class UserService
    {
        //public User LoggedUser = Home.LoggedInUser;
        HttpClient httpClient = new HttpClient() { BaseAddress = new Uri("https://localhost:44303") };


        public User CheckIfUserLoggedIn(string email, string password)
        {
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Login");

            string url = "api/Home/Login/";
            User user = new User();
            //try
            //{
            //mozda da se popuni user samo da bude validan new User() { Email = email, FirstName="Pera", LastName="Peric", Password=password, IndexNo="nesto", ProtocolNo="blah", UserID = 0 };
            //user.UserID = GetUserIDByEmail(email);// AllUsers().SingleOrDefault(u => u.Email.ToLower() == email.ToLower());
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authentication", user.UserID.ToString());
            user.Email = email.ToLower();
            user.Password = password;
            user.Approved = false;
            user.FirstName = "Test";
            user.LastName = "Test";
            user.ProtocolNo = "Test";
            user.UserID = 0;
            var httpRequestMessage = new HttpRequestMessage
            {
                Content = new StringContent(JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json")
            };
            httpRequestMessage.Method = HttpMethod.Put;
            httpRequestMessage.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //httpRequestMessage.Headers.Expect.Add(new NameValueWithParametersHeaderValue("application/json"));

            //professor.Exams.Add(exam);
            var resultLogin = httpClient.PutAsync(url, httpRequestMessage.Content).Result;
            if (!resultLogin.IsSuccessStatusCode)
            {
                throw new Exception(resultLogin.Content.ReadAsStringAsync().Result);
            }
            return JsonConvert.DeserializeObject<User>(resultLogin.Content.ReadAsStringAsync().Result) as User;
            //}
            //catch (Exception e1)
            //{
            //    throw e1;
            //}
            //try
            //{
            //    HttpResponseMessage result = httpClient.PostAsync(url).Result;
            //    usersStr = result.Content.ReadAsStringAsync().Result;
            //    Users = JsonConvert.DeserializeObject<List<User>>(usersStr);
            //    return Users.Find(x => email.Equals(x.Email) && password.Equals(x.Password));
            //}
            //catch (Exception e1)
            //{
            //    MessageBox.Show(e1.Message);
            //}
            return null;
        }

        //private int GetUserIDByEmail(string email)
        //{
        //    httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
        //    httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", Base64Encode(Home.LoggedInUser.Email.ToLower() + ":" + Home.LoggedInUser.Password));
        //    httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authentication", Home.LoggedInUser.UserID.ToString());

        //    string url = "api/Users/GetUserIDByMail/" + email;
        //    User user = new User();
        //    try
        //    {
        //        var httpRequestMessage = new HttpRequestMessage
        //        {
        //            Content = new StringContent(JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json")
        //        };
        //        httpRequestMessage.Method = HttpMethod.Get;
        //        httpRequestMessage.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //        //httpRequestMessage.Headers.Expect.Add(new NameValueWithParametersHeaderValue("application/json"));

        //        //professor.Exams.Add(exam);
        //        var resultLogin = httpClient.GetAsync(url).Result;
        //        if (!resultLogin.IsSuccessStatusCode)
        //        {
        //            throw new Exception(resultLogin.Content.ReadAsStringAsync().Result);
        //        }
        //        return JsonConvert.DeserializeObject<int>(resultLogin.Content.ReadAsStringAsync().Result);
        //    }
        //    catch (Exception e1)
        //    {
        //        throw e1;
        //    }
        //}

        internal List<User> GetAllUsers()
        {
            List<User> Users = new List<User>();
            string usersStr;
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", Base64Encode(Home.LoggedInUser.Email.ToLower() + ":" + Home.LoggedInUser.Password));
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authentication", Home.LoggedInUser.UserID.ToString());
            string url = "api/Users/GetUsers";
            try
            {
                HttpResponseMessage result = httpClient.GetAsync(url).Result;
                if (!result.IsSuccessStatusCode)
                {
                    throw new Exception(result.Content.ReadAsStringAsync().Result);
                }

                Home.HomeNotifier.ShowSuccess("Getting Users success");
                usersStr = result.Content.ReadAsStringAsync().Result;
                Users = JsonConvert.DeserializeObject<List<User>>(usersStr).ToList();
                return Users;
            }
            catch (Exception e1)
            {
                throw new Exception(e1.Message);
            }
            throw new Exception("Getting All Users Failed");
            return Users;
        }

        internal List<User> GetAllStudents()
        {
            List<User> Users;
            List<User> UsersFinal = new List<User>();
            string usersStr;
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", Base64Encode(Home.LoggedInUser.Email.ToLower() + ":" + Home.LoggedInUser.Password));
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authentication", Home.LoggedInUser.UserID.ToString());
            string url = "api/Users/GetUsers";
            try
            {
                HttpResponseMessage result = httpClient.GetAsync(url).Result;
                if (!result.IsSuccessStatusCode)
                {
                    throw new Exception("Getting All Students failed");
                }
                usersStr = result.Content.ReadAsStringAsync().Result;
                Users = JsonConvert.DeserializeObject<List<User>>(usersStr);
                for (int i = 0; i < Users.Count; i++)
                {
                    for (int j = 0; j < Users[i].Roles.Count; j++)
                    {
                        if (Users[i].Roles[j].RoleName.Equals("Student"))
                        {
                            UsersFinal.Add(Users[i]);
                        }
                    }
                }
                return UsersFinal;
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message);
            }
            throw new Exception("Getting All Students failed");
            return UsersFinal;
        }

        internal List<User> ApproveUser(User user)
        {
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", Base64Encode(Home.LoggedInUser.Email.ToLower() + ":" + Home.LoggedInUser.Password));
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authentication", Home.LoggedInUser.UserID.ToString());
            string urlE = "api/Users/ApproveUser/";
            //method below need refactoring
            try
            {

                var httpRequestMessage = new HttpRequestMessage
                {
                    Content = new StringContent(JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json")
                };

                httpRequestMessage.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var resultExam = httpClient.PutAsync(urlE, httpRequestMessage.Content).Result;
                if (!resultExam.IsSuccessStatusCode)
                {
                    throw new Exception(resultExam.Content.ReadAsStringAsync().Result);
                }


                /*new StringContent(JsonConvert.SerializeObject(exam))*/
                //HttpResponseMessage resultStudent = httpClient.PutAsJsonAsync(urlS, new StringContent(JsonConvert.SerializeObject(student))).Result;
                //usersStr = result.Content.ReadAsStringAsync().Result;
                //if(resultExam.StatusCode >= 200 && resultExam.StatusCode <= 204)
                //{
                //}
                return JsonConvert.DeserializeObject<List<User>>(resultExam.Content.ReadAsStringAsync().Result);
            }
            catch (Exception e1)
            {
                throw new Exception(e1.Message);
            }
        }

        internal List<User> ForbidUser(User user)
        {
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", Base64Encode(Home.LoggedInUser.Email.ToLower() + ":" + Home.LoggedInUser.Password));
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authentication", Home.LoggedInUser.UserID.ToString());
            string urlE = "api/Users/ForbidUser/";
            try
            {

                var httpRequestMessage = new HttpRequestMessage
                {
                    Content = new StringContent(JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json")
                };

                httpRequestMessage.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var resultExam = httpClient.PutAsync(urlE, httpRequestMessage.Content).Result;
                if (!resultExam.IsSuccessStatusCode)
                {
                    throw new Exception(resultExam.Content.ReadAsStringAsync().Result);
                }

                return JsonConvert.DeserializeObject<List<User>>(resultExam.Content.ReadAsStringAsync().Result);
            }
            catch (Exception e1)
            {
                throw new Exception(e1.Message);
            }
        }

        internal User LogOut()
        {
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", Base64Encode(Home.LoggedInUser.Email.ToLower() + ":" + Home.LoggedInUser.Password));
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authentication", Home.LoggedInUser.UserID.ToString());
            string urlE = "api/Users/LogOut/";
            try
            {

                var httpRequestMessage = new HttpRequestMessage
                {
                    Content = new StringContent(JsonConvert.SerializeObject(Home.LoggedInUser), Encoding.UTF8, "application/json")
                };

                httpRequestMessage.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var resultExam = httpClient.PutAsync(urlE, httpRequestMessage.Content).Result;
                if (!resultExam.IsSuccessStatusCode)
                {
                    throw new Exception(resultExam.Content.ReadAsStringAsync().Result);
                }

                return JsonConvert.DeserializeObject<User>(resultExam.Content.ReadAsStringAsync().Result);
            }
            catch (Exception e1)
            {
                throw e1;
            }
        }

        internal List<User> GetAllUsersWithRole(string role)
        {
            List<User> Users;
            //List<User> UsersFinal = new List<User>();
            string usersStr;
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", Base64Encode(Home.LoggedInUser.Email.ToLower() + ":" + Home.LoggedInUser.Password));
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authentication", Home.LoggedInUser.UserID.ToString());
            string url = "api/Users/GetUsersWithRole/"+"?role="+role;
            try
            {
                HttpResponseMessage result = httpClient.GetAsync(url).Result;
                if (!result.IsSuccessStatusCode)
                {
                    throw new Exception("Getting All " + role + "s failed");
                }
                usersStr = result.Content.ReadAsStringAsync().Result;
                Users = JsonConvert.DeserializeObject<List<User>>(usersStr);
                //for (int i = 0; i < Users.Count; i++)
                //{
                //    for (int j = 0; j < Users[i].Roles.Count; j++)
                //    {
                //        if (Users[i].Roles[j].RoleName.Equals(role))
                //        {
                //            UsersFinal.Add(Users[i]);
                //        }
                //    }
                //}
                return Users;
                //return UsersFinal;
            }
            catch (Exception e1)
            {
                Home.HomeNotifier.ShowError(e1.Message);
            }
            throw new Exception("Getting All " + role + "s failed");
            //return UsersFinal;
        }

        internal List<User> GetStudentsForExam(int id)
        {
            List<User> Users;
            List<User> UsersFinal = new List<User>();
            string usersStr;
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", Base64Encode(Home.LoggedInUser.Email.ToLower() + ":" + Home.LoggedInUser.Password));
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authentication", Home.LoggedInUser.UserID.ToString());
            string url = "api/Users/GetStudentsForExam/" + id;
            try
            {
                HttpResponseMessage result = httpClient.GetAsync(url).Result;

                if (!result.IsSuccessStatusCode)
                {
                    throw new Exception("Something went wrong with getting Students for Exam");
                }

                usersStr = result.Content.ReadAsStringAsync().Result;



                //Users = JsonConvert.DeserializeObject<List<User>>(usersStr).Where(x => x.Roles.Where(r => r.RoleName == "Professor") == null).ToList();
                Users = JsonConvert.DeserializeObject<List<User>>(usersStr);
                for (int i = 0; i < Users.Count; i++)
                {
                    for (int j = 0; j < Users[i].Roles.Count; j++)
                    {
                        if (Users[i].Roles[j].RoleName.Equals("Student"))
                        {
                            UsersFinal.Add(Users[i]);
                        }
                    }
                }
                return UsersFinal;
            }
            catch (Exception e1)
            {
                throw e1;
            }
            throw new Exception("Something went wrong with getting Students for Exam");
            return UsersFinal;
        }

        internal HttpStatusCode AddStudentProfToExam(User user, Exam exam)
        {
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", Base64Encode(Home.LoggedInUser.Email.ToLower() + ":" + Home.LoggedInUser.Password));
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authentication", Home.LoggedInUser.UserID.ToString());
            string urlE = "api/Exams/PutExam/" + exam.ExamID;

            try
            {
                exam.Users = new List<User>();
                exam.Users.Add(user);

                var httpRequestMessage = new HttpRequestMessage
                {
                    Content = new StringContent(JsonConvert.SerializeObject(exam), Encoding.UTF8, "application/json")
                };

                httpRequestMessage.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // var httpResponse = await _client.PostAsync("/contacts", httpRequestMessage.Content);

                //student.Exams.Add(exam);
                var resultExam = httpClient.PutAsync(urlE, httpRequestMessage.Content).Result;
                if (!resultExam.IsSuccessStatusCode)
                {
                    throw new Exception(resultExam.Content.ReadAsStringAsync().Result);
                }


                /*new StringContent(JsonConvert.SerializeObject(exam))*/
                //HttpResponseMessage resultStudent = httpClient.PutAsJsonAsync(urlS, new StringContent(JsonConvert.SerializeObject(student))).Result;
                //usersStr = result.Content.ReadAsStringAsync().Result;
                //if(resultExam.StatusCode >= 200 && resultExam.StatusCode <= 204)
                //{
                //}
                return resultExam.StatusCode;
            }
            catch (Exception e1)
            {
                throw new Exception(e1.Message);
            }
            return HttpStatusCode.BadRequest;
        }

        //async internal Task<HttpStatusCode> AddProfessorToExam(User professor, Exam exam)
        //{
        //    httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
        //    httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", Base64Encode(Home.LoggedInUser.Email.ToLower() + ":" + Home.LoggedInUser.Password));
        //    httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authentication", Home.LoggedInUser.UserID.ToString());
        //    string urlE = "api/Exams/" + exam.ExamID;
        //    string urlP = "api/Users/" + professor.UserID;

        //    try
        //    {
        //        exam.ProfessorIdentities = professor.UserID.ToString();
        //        exam.Users.Add(professor);
        //        //professor.Exams.Add(exam);
        //        HttpResponseMessage resultExam = await httpClient.PutAsync(urlE, new StringContent(JsonConvert.SerializeObject(exam)));
        //        if (!resultExam.IsSuccessStatusCode)
        //        {
        //            throw new Exception("Professor isn't assigned");
        //        }
        //        //HttpResponseMessage resultStudent = await httpClient.PutAsync(urlP, new StringContent(JsonConvert.SerializeObject(professor)));
        //        //usersStr = result.Content.ReadAsStringAsync().Result;
        //        //if(resultExam.StatusCode >= 200 && resultExam.StatusCode <= 204)
        //        //{
        //        //}
        //        return resultExam.StatusCode;
        //    }
        //    catch (Exception e1)
        //    {
        //        MessageBox.Show(e1.Message);
        //    }
        //    throw new Exception("Professor isn't assigned");
        //    return HttpStatusCode.BadRequest;
        //}

        internal User GetUser(int userID)
        {
            try
            {
                User User = new User();
                string userStr;
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", Base64Encode(Home.LoggedInUser.Email.ToLower() + ":" + Home.LoggedInUser.Password));
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authentication", Home.LoggedInUser.UserID.ToString());
                string url = "api/Users/GetUser/" + userID;

                HttpResponseMessage result = httpClient.GetAsync(url).Result;
                if (!result.IsSuccessStatusCode)
                {
                    throw new Exception(result.Content.ReadAsStringAsync().Result);
                }
                userStr = result.Content.ReadAsStringAsync().Result;
                User = JsonConvert.DeserializeObject<User>(userStr);
                return User;
            }
            catch (Exception e1)
            {
                throw new Exception(e1.Message);
            }
            throw new Exception("Getting User Failed");
        }
        
        internal HttpStatusCode SaveToken(Token token)
        {
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Login");
           
            string url = "api/Users/PostToken/";

            try
            {
                var httpRequestMessage = new HttpRequestMessage
                {
                    Content = new StringContent(JsonConvert.SerializeObject(token), Encoding.UTF8, "application/json")
                };

                httpRequestMessage.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpRequestMessage.Method = HttpMethod.Post;
                var result = httpClient.PostAsync(url, httpRequestMessage.Content).Result;

                if (!result.IsSuccessStatusCode)
                {
                    throw new Exception("Token didn't generate");
                }
                return result.StatusCode;
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message);
            }
            throw new Exception("Token didn't generate, aborting");
            return HttpStatusCode.BadRequest;
        }

        internal HttpResponseMessage RegisterUser(User user)
        {
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Login");
            string url = "api/Home/Register/";

            try
            {
                var httpRequestMessage = new HttpRequestMessage
                {
                    Content = new StringContent(JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json")
                };

                httpRequestMessage.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpRequestMessage.Method = HttpMethod.Post;
                var result = httpClient.PostAsync(url, httpRequestMessage.Content).Result;
                if (!result.IsSuccessStatusCode)
                {
                    throw new Exception(result.Content.ReadAsStringAsync().Result);
                }
                return result;
            }
            catch (Exception e1)
            {
                Login.LoginNotifier.ShowError(e1.Message);
            }
            //throw new Exception("User didn't register");
            throw new Exception("User didn't register");
        }

        private static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
    }
}
