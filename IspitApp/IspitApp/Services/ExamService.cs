﻿using Ionic.Zip;
using IspitApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace IspitApp.Services
{
    public class ExamService
    {
        HttpClient httpClient = new HttpClient() { BaseAddress = new Uri("https://localhost:44303") };
        private static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        public List<Exam> GetAllExams()
        {
            List<Exam> Exams = new List<Exam>();
            string examsStr;
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", Base64Encode(Home.LoggedInUser.Email.ToLower() + ":" + Home.LoggedInUser.Password));
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authentication", Home.LoggedInUser.UserID.ToString());
            string url = "api/Exams/";
            try
            {
                HttpResponseMessage result = httpClient.GetAsync(url).Result;
                if (!result.IsSuccessStatusCode)
                {
                    throw new Exception(result.Content.ReadAsStringAsync().Result);
                }
                //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(result.Headers.GetValues("Authorization").ToList()[0]);
                examsStr = result.Content.ReadAsStringAsync().Result;
                Exams = JsonConvert.DeserializeObject<List<Exam>>(examsStr);
                return Exams;
            }
            catch (Exception e1)
            {
                //MessageBox.Show(e1.Message);
                throw new Exception(e1.Message);
            }
            throw new Exception("Exems not found");
        }

        public List<Exam> GetAllExamsForUser()
        {
            List<Exam> Exams = new List<Exam>();
            string examsStr;
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", Base64Encode(Home.LoggedInUser.Email.ToLower() + ":" + Home.LoggedInUser.Password));
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authentication", Home.LoggedInUser.UserID.ToString());
            string url = "api/Exams/AllExamsForUser/" + Home.LoggedInUser.UserID;
            try
            {
                var httpRequestMessage = new HttpRequestMessage
                {
                    Content = new StringContent(JsonConvert.SerializeObject(Home.LoggedInUser), Encoding.UTF8, "application/json")
                };
                httpRequestMessage.Method = HttpMethod.Get;
                httpRequestMessage.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage result = httpClient.GetAsync(url).Result;

                if (!result.IsSuccessStatusCode)
                {
                    throw new Exception(result.Content.ReadAsStringAsync().Result);
                }
                examsStr = result.Content.ReadAsStringAsync().Result;
                Exams = JsonConvert.DeserializeObject<List<Exam>>(examsStr);
                return Exams;
            }
            catch (Exception e1)
            {
                throw new Exception(e1.Message);
            }

            return Exams;
        }


        internal List<User> UnEnrollStudentFromExam(Exam examOnSeeUsers, Button button)
        {
            try
            {
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", Base64Encode(Home.LoggedInUser.Email.ToLower() + ":" + Home.LoggedInUser.Password));
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authentication", Home.LoggedInUser.UserID.ToString());
                string urlE = "api/Exams/UnEnroll/" + examOnSeeUsers.ExamID;

                var httpRequestMessage = new HttpRequestMessage
                {
                    Content = new StringContent(JsonConvert.SerializeObject(button.Name), Encoding.UTF8, "application/json")
                };

                httpRequestMessage.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage resultExam = httpClient.PutAsync(urlE, httpRequestMessage.Content).Result;/*new StringContent(JsonConvert.SerializeObject(exam))*/


                if (!resultExam.IsSuccessStatusCode)
                {
                    throw new Exception(resultExam.Content.ReadAsStringAsync().Result);
                }

                return JsonConvert.DeserializeObject<List<User>>(resultExam.Content.ReadAsStringAsync().Result);
            }
            catch (Exception e1)
            {
                throw e1;
            }
            return new List<User>();
        }

        internal HttpResponseMessage CheckIn(Exam selectedExam)
        {
            try
            {
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", Base64Encode(Home.LoggedInUser.Email.ToLower() + ":" + Home.LoggedInUser.Password));
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authentication", Home.LoggedInUser.UserID.ToString());
                string urlE = "api/Exams/PutCheckIn/" + selectedExam.ExamID;

                var httpRequestMessage = new HttpRequestMessage
                {
                    Content = new StringContent(JsonConvert.SerializeObject(Home.LoggedInUser), Encoding.UTF8, "application/json")
                };

                httpRequestMessage.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage resultCheckIn = httpClient.PutAsync(urlE, httpRequestMessage.Content).Result;/*new StringContent(JsonConvert.SerializeObject(exam))*/
                if (resultCheckIn.StatusCode == HttpStatusCode.Forbidden)
                {
                    return resultCheckIn;
                }
                else if (!resultCheckIn.IsSuccessStatusCode)
                {
                    throw new Exception(resultCheckIn.Content.ReadAsStringAsync().Result);
                }
                return resultCheckIn;
            }
            catch (Exception e2)
            {
                throw e2;
            }
            throw new Exception("checking in didn't succeed");
        }

        internal List<CheckIn> GetFiles(Exam examOnDownloadFiles)
        {
            try
            {
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", Base64Encode(Home.LoggedInUser.Email.ToLower() + ":" + Home.LoggedInUser.Password));
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authentication", Home.LoggedInUser.UserID.ToString());
                string urlE = "api/Exams/PutFiles/";

                var httpRequestMessage = new HttpRequestMessage
                {
                    Content = new StringContent(JsonConvert.SerializeObject(examOnDownloadFiles), Encoding.UTF8, "application/json")
                };

                httpRequestMessage.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage resultCheckIn = httpClient.PutAsync(urlE, httpRequestMessage.Content).Result;/*new StringContent(JsonConvert.SerializeObject(exam))*/
                if (!resultCheckIn.IsSuccessStatusCode)
                {
                    throw new Exception(resultCheckIn.Content.ReadAsStringAsync().Result);
                }
                return JsonConvert.DeserializeObject<List<CheckIn>>(resultCheckIn.Content.ReadAsStringAsync().Result);
            }
            catch (Exception e2)
            {
                throw e2;
            }
            throw new Exception("Getting files didn't succeed");
        }

        internal HttpResponseMessage CheckOut()
        {
            try
            {
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", Base64Encode(Home.LoggedInUser.Email.ToLower() + ":" + Home.LoggedInUser.Password));
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authentication", Home.LoggedInUser.UserID.ToString());
                string urlE = "api/Exams/PutCheckOut/" + ExamRead.SelectedExam.ExamID;

                var httpRequestMessage = new HttpRequestMessage
                {
                    Content = new StringContent(JsonConvert.SerializeObject(Home.LoggedInUser), Encoding.UTF8, "application/json")
                };

                httpRequestMessage.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage resultExam = httpClient.PutAsync(urlE, httpRequestMessage.Content).Result;/*new StringContent(JsonConvert.SerializeObject(exam))*/
                if (!resultExam.IsSuccessStatusCode)
                {
                    throw new Exception(resultExam.Content.ReadAsStringAsync().Result);
                }
                return resultExam;
            }
            catch (Exception e2)
            {
                throw e2;
            }
            throw new Exception("checking in didn't succeed");
        }


        internal HttpResponseMessage UploadFile(CheckIn checkIn)
        {
            try
            {
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", Base64Encode(Home.LoggedInUser.Email.ToLower() + ":" + Home.LoggedInUser.Password));
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authentication", Home.LoggedInUser.UserID.ToString());
                string urlE = "api/Exams/UploadFile/";

                var httpRequestMessage = new HttpRequestMessage
                {
                    Content = new StringContent(JsonConvert.SerializeObject(checkIn), Encoding.UTF8, "application/json")
                };

                httpRequestMessage.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage resultCheckIn = httpClient.PutAsync(urlE, httpRequestMessage.Content).Result;/*new StringContent(JsonConvert.SerializeObject(exam))*/
                if (!resultCheckIn.IsSuccessStatusCode)
                {
                    throw new Exception(resultCheckIn.Content.ReadAsStringAsync().Result);
                }
                return resultCheckIn;
            }
            catch (Exception e2)
            {
                throw e2;
            }
            throw new Exception("upload file didn't succeed");
        }

        internal HttpResponseMessage UploadFileProf(CheckIn checkIn)
        {
            try
            {
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", Base64Encode(Home.LoggedInUser.Email.ToLower() + ":" + Home.LoggedInUser.Password));
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authentication", Home.LoggedInUser.UserID.ToString());
                string urlE = "api/Exams/UploadFileProf/";

                var httpRequestMessage = new HttpRequestMessage
                {
                    Content = new StringContent(JsonConvert.SerializeObject(checkIn), Encoding.UTF8, "application/json")
                };

                httpRequestMessage.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage resultCheckIn = httpClient.PostAsync(urlE, httpRequestMessage.Content).Result;/*new StringContent(JsonConvert.SerializeObject(exam))*/
                if (!resultCheckIn.IsSuccessStatusCode)
                {
                    throw new Exception(resultCheckIn.Content.ReadAsStringAsync().Result);
                }
                return resultCheckIn;
            }
            catch (Exception e2)
            {
                throw e2;
            }
            throw new Exception("upload file didn't succeed");

        }
    }
}
