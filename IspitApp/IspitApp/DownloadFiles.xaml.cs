﻿using IspitApp;
using IspitApp.Models;
using IspitApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ToastNotifications;
using ToastNotifications.Messages;

namespace ExamApp
{
    /// <summary>
    /// Interaction logic for DownloadFiles.xaml
    /// </summary>
    public partial class DownloadFiles : Page
    {

        public List<CheckIn> FilesOnExam { get; set; }
        public List<CheckIn> FilesOnExamReserve { get; set; }
        public Exam ExamOnDownloadFiles { get; set; }
        public TextBox SearchTextBox { get; set; }
        public List<CheckIn> usersFilesReserve { get; private set; }

        private ExamService examService = new ExamService();
        private UserService userService = new UserService();
        public Notifier HomeNotifier { get; set; }
        public DownloadFiles()
        {
            InitializeComponent();
            CreateSearchBar();
            this.Loaded += DownloadFiles_Loaded;
            this.SizeChanged += DownloadFiles_SizeChanged;
        }

        private void CreateSearchBar()
        {
            Label lblTitle = new Label();
            SearchTextBox = new TextBox();
            SearchTextBox.SetBinding(TextBox.WidthProperty, new Binding("SearchBoxWidth"));
            SearchTextBox.KeyUp += SearchTextBox_KeyUp;
            //searchTextBox.Width = SearchBoxWidth;
            lblTitle.Content = new StackPanel();
            (lblTitle.Content as StackPanel).Orientation = Orientation.Horizontal;
            (lblTitle.Content as StackPanel).Children.Add(new Label() { Content = "Search by file name, date, student :" });
            (lblTitle.Content as StackPanel).Children.Add(SearchTextBox);
            (lblTitle.Content as StackPanel).Children.Add(new Label() { Content = "All Files - " + ExamRead.SelectedExam.ExamName });

            Grid.SetColumn(lblTitle, 0);
            Grid.SetRow(lblTitle, 0);
            searchScroll.Content = lblTitle;
        }

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            SearchTextBox = (sender as TextBox);

            Filter();

            CreateLayout();
        }
        private void CreateLayout()
        {
            seeFilesGrid.Children.Clear();
            seeFilesGrid.ColumnDefinitions.Clear();
            seeFilesGrid.RowDefinitions.Clear();
            ColumnDefinition colIndexNo = new ColumnDefinition();
            colIndexNo.Width = new GridLength(2, GridUnitType.Star);
            ColumnDefinition colFullName = new ColumnDefinition();
            colFullName.Width = new GridLength(6, GridUnitType.Star);
            ColumnDefinition colBtnDownload = new ColumnDefinition();
            colBtnDownload.Width = new GridLength(1, GridUnitType.Star);
            ColumnDefinition colBtnDel = new ColumnDefinition();
            colBtnDel.Width = new GridLength(1, GridUnitType.Star);
            seeFilesGrid.ColumnDefinitions.Add(colIndexNo);
            seeFilesGrid.ColumnDefinitions.Add(colFullName);
            seeFilesGrid.ColumnDefinitions.Add(colBtnDownload);
            seeFilesGrid.ColumnDefinitions.Add(colBtnDel);
            for (int i = 0; i < FilesOnExam.Count; i++)
            {
                RowDefinition row = new RowDefinition();
                row.Height = new GridLength(100, GridUnitType.Pixel);
                seeFilesGrid.RowDefinitions.Add(row);
            }

            SolidColorBrush background = new SolidColorBrush(Colors.White);
            for (int i = 0; i < FilesOnExam.Count; i++)
            {
                if (FilesOnExam[i].FilePath == null) Home.HomeNotifier.ShowWarning("Some students didn't checkout yet");
                if ((background.Color.ToString() == Colors.LightBlue.ToString())) { 
                    background = new SolidColorBrush(Colors.White);
                }
                else
                {
                    background = new SolidColorBrush(Colors.LightBlue);
                }
                Border border0 = new Border() { BorderBrush = new SolidColorBrush(Colors.Black), Background = background };
                Border border1 = new Border() { BorderBrush = new SolidColorBrush(Colors.Black), Background = background };
                Border border2 = new Border() { BorderBrush = new SolidColorBrush(Colors.Black), Background = background };
                Grid.SetRow(border0, i);
                Grid.SetRow(border1, i);
                Grid.SetRow(border2, i);
                Grid.SetColumn(border0, 0);
                Grid.SetColumn(border1, 1);
                Grid.SetColumn(border2, 2);
                seeFilesGrid.Children.Add(border0);
                seeFilesGrid.Children.Add(border1);
                seeFilesGrid.Children.Add(border2);

                User user = userService.GetUser(FilesOnExam[i].UserID);
                string displayName = "";
                string propName = "";
                if (FilesOnExam[i].FilePath == "NA" || FilesOnExam[i].FilePath == null)
                {
                    displayName = user.FullName();
                    propName = user.FullName().Replace(" ", "_");
                }
                else
                {
                    displayName = FilesOnExam[i].FilePath.Split('\\').Last().Replace("_"," ");
                    propName = FilesOnExam[i].FilePath.Split('\\').Last().Replace(" ", "_").Replace("-", "_").Replace(".", " ").Split(' ')[0];
                }


                Label lblIndexNo = new Label() { Content = FilesOnExam[i].CheckInID.ToString() };
                Grid.SetRow(lblIndexNo, i);
                Grid.SetColumn(lblIndexNo, 0);
                seeFilesGrid.Children.Add(lblIndexNo);

                Label lblFullName = new Label() { Content = displayName };
                Grid.SetRow(lblFullName, i);
                Grid.SetColumn(lblFullName, 1);
                seeFilesGrid.Children.Add(lblFullName);

                
                Button btnDownload = new Button() { Content = "Download File", Name = "Download_" +   propName };
                btnDownload.Click += BtnDownload_Click;
                btnDownload.FontSize = 10;
                Grid.SetRow(btnDownload, i);
                Grid.SetColumn(btnDownload, 2);
                seeFilesGrid.Children.Add(btnDownload);

                Button btnDelete = new Button() { Content = "Delete File", Name = "Delete_" + propName };
                btnDelete.Click += BtnDelete_Click;
                btnDelete.FontSize = 10;
                Grid.SetRow(btnDelete, i);
                Grid.SetColumn(btnDelete, 3);
                seeFilesGrid.Children.Add(btnDelete);

            }
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void BtnDownload_Click(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void Filter()
        {
            if (FilesOnExamReserve == null)
            {
                CheckIn[] chs = new CheckIn[FilesOnExam.Count];
                FilesOnExam.CopyTo(chs);
                FilesOnExamReserve = chs.ToList();   
            }
            if (SearchTextBox.Text == "")
            {
                FilesOnExam = FilesOnExamReserve.Where(u => u.FilePath != "0").ToList();
            } else if(SearchTextBox.Text.ToLower() == "NA".ToLower())
            {
                FilesOnExam = FilesOnExamReserve.Where(u => u.FilePath == "NA"  || u.FilePath.ToLower().Contains("NA".ToLower())).ToList();
            } else if(SearchTextBox.Text.ToLower() == "null".ToLower())
            {
                FilesOnExam = FilesOnExamReserve.Where(u => u.FilePath == null).ToList();
            }
            else
            {
                FilesOnExam = FilesOnExamReserve.Where(u => u.FilePath.ToLower().Contains(SearchTextBox.Text)).ToList();
            }
        }

        private void DownloadFiles_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            SearchTextBox.Width = e.NewSize.Width / 4;
        }

        private void DownloadFiles_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                FilesOnExam = examService.GetFiles(ExamOnDownloadFiles);
                CreateLayout();
            }
            catch(Exception ex) { Home.HomeNotifier.ShowError(ex.Message); }
        }
    }
}
