﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ExamApp;
using ExamApp.Models;
using IspitApp.Models;

namespace IspitApp.Models
{
    public class Exam : NotifyPropertyChanged
    {
        public int ExamID { get; set; }
        public string ExamName { get; set; }
        public int YearOfStudies { get; set; }
        public int Semester { get; set; }
        public string LevelOfStudies { get; set; }
        public string ExamImagePath { get; set; }
        public List<Chat> Chats { get; set; }
        public List<CheckIn> CheckIns { get; set; }
        public List<User> Users { get; set; }
        public List<Department> Departments { get; set; }

        public override string ToString()
        {
            return ExamName;
        }
    }
}