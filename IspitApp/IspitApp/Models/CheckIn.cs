﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IspitApp.Models
{
    public class CheckIn
    {
        public int CheckInID { get; set; }
        public DateTime CheckInDateTime { get; set; }
        public DateTime? CheckOutDateTime { get; set; }
        public int ExamID { get; set; }
        //public Exam Exam { get; set; }
        public int UserID { get; set; }
        //public User User { get; set; }
        public string FilePath { get; set; }

    }

}