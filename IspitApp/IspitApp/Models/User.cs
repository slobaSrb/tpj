﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IspitApp.Models
{
    public class User
    {
        public int UserID { get; set; }
        public string IndexNo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ProtocolNo { get; set; }
        public bool Approved { get; set; }
        public string FullName() { return this.FirstName + " " + this.LastName; }
        public List<Chat> Chats { get; set; }
        public List<Exam> Exams { get; set; }
        public List<CheckIn> CheckIns { get; set; }
        public List<Role> Roles { get; set; }
        public override string ToString()
        {
            return IndexNo==null ? "" : IndexNo+" / "+FullName();
        }
    }

}