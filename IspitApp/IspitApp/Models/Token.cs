﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IspitApp.Models
{
    public class Token
    {
        public int TokenID { get; set; }
        public string TokenValue { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int UserID { get; set; }

    }

}