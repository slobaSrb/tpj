﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IspitApp.Models
{
    public class Chat
    {
        public int ChatID { get; set; }
        public string Message { get; set; }
        public int ExamID { get; set; }
        public virtual Exam Exam { get; set; }
        public int UserID { get; set; }
        public virtual User User { get; set; }
        public DateTime DateShown { get; set; }

    }

}